### **CosmicNotes:** *Write your daily lab notebook entries within Galaxy.*
CosmicNotes is an electronic lab notebook plugin for Galaxy. It is integrated via Galaxy's
visualizations framework and provides two new visualizations for every
dataset that enable the creation of richly formatted lab book entries
through a web editor. One visualization supports the annotation of
individual datasets beyond what is possible with Galaxy's built in
annotation feature. The second lets users maintain lab book pages for
whole histories with the option to cross-link to dataset annotations.
CosmicNotes lab book entries are version controlled on a daily basis
(through using Git in the background). In combination with an integrated
diff viewer, this allows for fully editable lab book entries until final
submission of a project, while supporting full tracking of all changes.
An overview page lists lab books associated with published histories on
the server and offers the option to search by history names, authors
and/or CosmicNotes projects, which can be used to group lab books and
histories. Work in progress includes additional features such as
export/import functionality and full-text search support.

### Installation instructions: ###

To install CosmicNotes, simply copy the repository directly in the vizualisations directory of your Galaxy instance (galaxy/config/plugins/visualizations).
CosmicNotes is still in an early development stage, so it is currently necessary to etablished a postgreSQL database (if not alreary set up for Galaxy) with the name 'eln'. It is planned in the near future to automate this step and additionally support SQLite.

### Dependencies: ###
[Pandoc](http://pandoc.org/)

[Git](https://git-scm.com/)