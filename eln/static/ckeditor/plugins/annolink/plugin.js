CKEDITOR.plugins.add( 'annolink', {	//add a new plugin to ckeditor
    icons: 'annolink',			//icon have to be in he correct dir
    init: function( editor ) {		//initilaze plugin
        editor.addCommand( 'annolink', new CKEDITOR.dialogCommand( 'annolinkDialog' ) ); // adds new Command to the plugin, in this case a new Dialog
        editor.ui.addButton( 'Annolink', { // add Button to the editor
            label: 'Annotation Link',
            command: 'annolink',
            toolbar: 'links,10'		//sets the toolbar group and the position in the toolbar
        });

        CKEDITOR.dialog.add( 'annolinkDialog', this.path + 'dialogs/annolink.js' ); //sets up the dialoge path 
    }
});
