    
parsed_dataset_info = JSON.parse(annolink_transfer);

CKEDITOR.dialog.add( 'annolinkDialog', function( editor ) {  // entry for plugin.js
    return {
        title: 'Annotation Link',
        minWidth: 400,
        minHeight: 200,
        contents: [
        {
            id: 'tab1',                 //window tab
            label: 'Current History',
            elements: [
                
                {
                type : 'text',
                id : 'contents',
                label : 'Displayed Text', 
                setup: function( selection ) {
                     this.setValue( selection );
                    },          
                },
                {
                type: 'select',           //elements
                id: 'checkbox',
                label: 'Please select your Dataset:',
                items: [  ],               // objects in the checkbox
//                validate: CKEDITOR.dialog.validate.notEmpty( "Dataset field cannot be empty." ),                   
                onShow: function() {  
                    this.clear();                    
                    var i;                  
                    for (i = 0; i < parsed_dataset_info['dataset_number'].length; i++) {    //append items with ele of dataset_array, set the value to function as a index 
                        this.add(parsed_dataset_info['dataset_number'][i], [i]);                //.add(lable, value)            
                        }
                    }
                }
            ]
            
        },
        {
            id: 'tab2',                 //window tab
            label: 'Other Histories',
            elements: [
            
                {
                type : 'text',
                id : 'contents',
                label : 'Displayed Text',
                setup: function( selection ) {
                this.setValue( selection );
                    }    
                },
         
                {
                type: 'select',           //elements
                id: 'checkbox_hist',
                label: 'Please select your history:',
                items: [  ],               // objects in the checkbox        
                onShow: function() { 
                    history_select = this;
                    history_select.clear();
                    
                    $.each(other_hist_data, function(i, val) {
                        //console.log(val[0]);
                        if (!$.isEmptyObject(val[0])) {
                            history_select.add(val[1], [i]);   
                        }       
                    
                        })

                    },
                onChange: function () {
                
                    anno_select.clear();
                    hist_selection = history_select.getValue();                      
                    anno_obj = other_hist_data[hist_selection][0]
                                            
                    $.each(anno_obj, function(anno_id, val) {
                        anno_select.add(val[0], [anno_id]);   
                    })
                
                    }
                                                  
                },
                                        
                {
                type: 'select',           //elements
                id: 'checkbox_anno',
                label: 'Please select your annotation:',
                items: [  ],               // objects in the checkbox        
                onShow: function() { 
                    anno_select = this;
                    anno_select.clear();
                    hist_selection = history_select.getValue();                      
                    anno_obj = other_hist_data[hist_selection][0]
                                            
                    $.each(anno_obj, function(anno_id, val) {
                        anno_select.add(val[0], [anno_id]);   
                    })

                    }                                  
                     
                }
            
            ]    
        },
        
        {
            id: 'tab3',                 //window tab
            label: 'No History',
            elements: [
                
                {
                type: 'html',
                html: 'There are no annotations in the current history, neither in histories of related projects',       
                },
            ]
            
        }       
           
        ],

        onShow: function() {
            var selection = editor.getSelection().getSelectedText();
            this.setupContent( selection ); 
     
     
            if (typeof other_hist_data === 'undefined' || !other_hist_data ) {
                url = root+'visualization/show/eln?'+
                      'dataset_id='+encoded_dataset_id;
                address =  window.location.protocol + "//" + window.location.host + root;
                $.ajax( {
                    url:    url,
                    async:  false,
                    type:   'GET',
                    dataType: "json",
                    data: {annolink: historyId,
                           root: address,
                           user: username,
                        }, 
                    success: function (data) {
                        other_hist_data = data;
                        }
                });
            }
      
            // hide tab1 when there is no annotation in the displayed hist. 
            if ($.isEmptyObject(parsed_dataset_info['dataset_name'])) {
                this.hidePage( 'tab1' );
            }
            
            // hide tab1 when the current hist is not the displayed hist  
            if ( diverse_hist ) {
                this.hidePage( 'tab1' );      //Hide tab.
            }

            if ($.isEmptyObject(other_hist_data)) {
                this.hidePage( 'tab2' );      //Hide tab.
            }
             
            if ((diverse_hist || $.isEmptyObject(other_hist_data)) && $.isEmptyObject(parsed_dataset_info['dataset_name'])) {
                this.hidePage( 'tab1' );
                this.hidePage( 'tab2' ); 
            }
            else {
                this.hidePage( 'tab3' );
            }        
        },            


        onCancel: function () {
            this.showPage( 'tab1' );  
            this.showPage( 'tab2' );  
            this.showPage( 'tab3' );   
            editor.insertHtml( '' );  // writing empty string into document to close dialog 
            

        },


        onOk: function() {          // user clicks OK

            var CurrObj = CKEDITOR.dialog.getCurrent();
            var current_tab = CurrObj.definition.dialog._.currentTabId;
            
            this.showPage( 'tab1' );  
            this.showPage( 'tab2' );  
            this.showPage( 'tab3' );  

            if(current_tab == 'tab1') {
                var textarea_content = this.getContentElement( 'tab1', 'contents' ).getValue();
                var checkbox_value = this.getContentElement( 'tab1', 'checkbox' ).getValue();
                if (textarea_content != ""){
                    editor.insertHtml( '<a href='+parsed_dataset_info['dataset_url'][checkbox_value]+'>'+textarea_content+'</a>' );  //creating link                           
                }
                else {
                    editor.insertHtml( '<a href='+parsed_dataset_info['dataset_url'][checkbox_value]+'>'+'Dataset '+parsed_dataset_info['dataset_number'][checkbox_value]+'</a>' );
                }
            } 
            
            else  if (current_tab == 'tab2') {
                var text_content = this.getContentElement( 'tab2', 'contents' ).getValue();
                var out_hist_id = this.getContentElement( 'tab2', 'checkbox_hist' ).getValue();
                var out_anno_id = this.getContentElement( 'tab2', 'checkbox_anno' ).getValue();
                var url = other_hist_data[out_hist_id][0][out_anno_id][1]
                
                if (text_content != ''){
                    
                    var link_name = text_content
                    editor.insertHtml( '<a href='+url+'>'+link_name+'</a>' );
                    
                }
                else {
                    
                    var link_name = other_hist_data[out_hist_id][0][out_anno_id][0]
                    editor.insertHtml( '<a href='+url+'>'+link_name+'</a>' );
                }
            
            }
            
            else {
                editor.insertHtml( '' );  // writing empty string into document to close dialog 
            }
            
        
        }
    };
});
//    }

      

