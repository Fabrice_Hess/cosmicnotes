#!/usr/bin/env python2.7

import urllib2
import json
import os
import shutil
import zipfile
import pypandoc

import eln_util
import experiment
import db_modules
import githandling

def rm_file_extension (name, extension) :
    """ Removes the file extension, when existing and identical to the received extension.
    """
    file_name,file_extension = os.path.splitext(name)
    if file_extension:
        if extension == file_extension[1:].lower():
            return file_name
        else:
            return name
    else:
        return name


def get_file_paths (path, type):
    """ Checks path for html files and returns a sorted list of names.
    """
    file_paths = []
    if os.path.isdir(path):
        for file in os.listdir(path):
            if file.endswith("."+type):
                file_paths.append(os.path.join(path,file))
        file_paths.sort()
    return file_paths
  
  
def generate_dataset_legend (dataset_list):

    dataset_legend = ''
    for dataset in dataset_list:
        dataset_legend += '<tr>'
        dataset_legend += '<td><a href="./'+dataset.export_path+'" title="'+dataset.name+'">'+dataset.name+'</a></td>'
        if dataset.has_annotation():
            dataset_legend += '<td><a class="annotation_button" href="./'+dataset.anno_export_path+'" title="'+dataset.name+'">Annotation</a></td>'
        dataset_legend += '</tr>'

    dataset_legend_template= """
        <div class='seperator'> Dataset Overview</div>
        <div class ='annotation_overview'>
            <table class='dataset_table'>
            {dataset_legend}
            </table>
        </div>
    """
    dataset_legend_template = unicode(dataset_legend_template)
    return dataset_legend_template.format(dataset_legend = dataset_legend)

  
  
def generate_entry_html (entry, content, dropdown_options, i=0):

    if entry.is_modified():
        last_modified_template = """
        <font color="red">Last Modify: {last_modify_date}</font> 
        """
        last_modified_template = unicode(last_modified_template)
        last_modify = last_modified_template.format(last_modify_date=entry.date_last_modified)
        
        dd_template = """
        <div class='dropdown'>
            <form>
                <select onchange='ChangeContent({i})' id='dropdown_{i}'>
                    ${dropdown_option_tag}
                </select>
            </form>
        </div>
        """
        dd_template = unicode(dd_template)
        dropdown_box = dd_template.format(dropdown_option_tag = dropdown_options,
                                          i = i,)
    else:
        last_modify = ''
        dropdown_box = ''
    
    template = """
    <div class='entry'>
        <div class= 'entry_header'>
            <div class='date'>
                Creation Date: {creation_date} <br>
                {last_modify}
            </div>            
            {dropdown_box}
        </div>
        <div class= 'entry_content_box'> 
            <div class= 'entry_content' id='content_{i}'>
                {content}
            </div>
        </div>
    </div>
    """    
    
    template = unicode(template)
    #content = content.encode('utf-8')

    return template.format(creation_date = entry.date_created,
                                           content = content,
                                           last_modify = last_modify,
                                           dropdown_box = dropdown_box,
                                           i = i,) 


def notebook_template_html (history_title, export_css, entry_html, old_versions_json, dataset_legend_html):
    template = """
    <!DOCTYPE HTML>
    <html>
        <head> 
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <style>
                {export_css}
            </style>
        </head>        
        <body>            
            <script>
            var old_versions = {old_versions_json};            

            function ChangeContent(number) {{            
                var content_box = document.getElementById("content_"+number);
                var old_version = old_versions[number];
                var dd = document.getElementById("dropdown_"+number);
                var dd_value = dd.options[dd.selectedIndex].value;
                content_box.innerHTML = old_version[dd_value];
            }}            
            </script>
            <div class='title'> {history_title} </div>
            {entry_html}   
            {dataset_legend_html}
        </body>
    </html>
    """
    template = unicode(template)
    return template.format(history_title = history_title,
                                           export_css = export_css,
                                           entry_html = entry_html,
                                           old_versions_json = old_versions_json,
                                           dataset_legend_html = dataset_legend_html,)       


def generate_anno_html (anno_entry, old_ver_json, annotation, anno_export_css):
    
    anno_template = """
    <!DOCTYPE HTML>
    <html>
        <head> 
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <style>
                {anno_export_css}
            </style>
        </head>        
        <body>            
            <script>
            var old_versions = {old_ver_json};            

            function ChangeContent(number) {{            
                var content_box = document.getElementById("content_0");
                var dd = document.getElementById("dropdown_0");
                var dd_value = dd.options[dd.selectedIndex].value;
                content_box.innerHTML = old_versions[dd_value];
            }}            
            </script>
            <div class='title'>Annotation: {dataset_name} </div>
            {anno_entry}   

        </body>
    </html>
    """
    anno_template = unicode(anno_template)
    return anno_template.format(anno_entry = anno_entry,
                                                anno_export_css = anno_export_css,
                                                old_ver_json = old_ver_json,
                                                dataset_name = annotation.dataset_name,
                                               )   


def update_annolink (entry_text_html, dataset_list, type):
    
    search_string ='href="/visualization/show/annotation?dataset_id='
    len_search = len(search_string)
    
    if type == 'anno':
        replace_string= 'href="./'
    elif type == 'hist':
        replace_string= 'href="./annotations/'
    
    link_list=[]
    pos = 0
    while pos > -1:    
        pos = entry_text_html.find(search_string, pos+1)    
        if pos > -1:        
            pos_id= pos + len_search
            pos_end_tag= entry_text_html.find('>', pos_id)
            anno_id = entry_text_html[pos_id:pos_end_tag-1]
            
            for dataset in dataset_list:
                if dataset.id == anno_id:                                        
                    anno_file_name = dataset.anno_file_html                 

            link_list.append(  (entry_text_html[pos:pos_end_tag], replace_string + anno_file_name + '"' ) )
    
    for original_link, replace_link in link_list:
        entry_text_html = entry_text_html.replace(original_link, replace_link)
        
    return entry_text_html


class ExportDataset (object):

    def __init__(self, dataset_dict, trans):
        
        self.url = dataset_dict['url']
        self.extension = dataset_dict['extension']
        self.name = self.rm_file_extension(dataset_dict['name'],dataset_dict['extension'])
        self.id = dataset_dict['id']
        self.export_path = 'datasets/'+self.name+'.'+self.extension
        self.anno_export_path = ''
        self.anno_file_html = ''
        self.anno_content = ''
        self.anno_id = ''
    
    def add_annotation (self, anno_file_content, dataset_list, anno_id, file_name):
        header, file_txt = anno_file_content.split('\n', 1)
        self.anno_file_html = file_name + '.html'
        self.anno_export_path = 'annotations/'+ self.anno_file_html
        self.anno_id = anno_id


    def has_annotation (self):
        return bool(self.anno_content)
    
    def rm_file_extension (self, name, extension) :
        """ Removes the file extension, when existing and identical to the received extension.
        """
        file_name,file_extension = os.path.splitext(name)
        if file_extension:
            if extension.lower() == file_extension[1:].lower():
                return file_name
            else:
                return name
        else:
            return name


def export (enc_hist_id, root, trans):
    
    hist_id = str(trans.security.decode_id(enc_hist_id))
    url = root+'api/histories/'+enc_hist_id+'/contents';  
    headers = {'Cookie' : 'galaxysession='+trans.get_cookie()}
    request = urllib2.Request(url, headers = headers)    
    response = urllib2.urlopen(request)
    history_dict = json.loads(response.read())

    dataset_list = []    
    for dataset in history_dict:
        if dataset['deleted'] or dataset['purged']:
            pass
        else:
            dataset_list.append(ExportDataset(dataset, trans))

    hist_name = db_modules.get_hist_name(hist_id)

    export_path = eln_util.ElnConfig().get_export_path()
    export_full_path = os.path.join(export_path, hist_name+'.zip')
    zf = zipfile.ZipFile(export_full_path, 
                        mode='w',
                        compression=zipfile.ZIP_DEFLATED, 
                        )
    try:
        # save datasets in zipfile
        for dataset in dataset_list:
            url = os.path.join(root+dataset.url,'display');  
            headers = {'Cookie' : 'galaxysession='+trans.get_cookie()}        
            request = urllib2.Request(url, headers = headers)    
            response = urllib2.urlopen(request)
            content = response.read()
            zf.writestr(dataset.export_path, content)
            
            
        # save annotations        
        archive_path = eln_util.archive_path()
        hist_path = os.path.join(archive_path, hist_id)
        anno_paths = get_file_paths(os.path.join(hist_path, 'annotations'), 'html')
        anno_dir_path = os.path.join(hist_path,'annotations')
        
        gitcall_anno = githandling.GitCall(anno_dir_path)
        temp_anno_path = os.path.join(anno_dir_path,"temp")
              
        for path in anno_paths:
            file_name,file_extension = os.path.splitext(os.path.basename(path))
            file_id = trans.security.encode_id(file_name)
            
            for dataset in dataset_list:
                if file_id  == dataset.id:
                    with open(path, 'r') as anno_file:
                        anno_file_content = anno_file.read()
                    dataset.add_annotation(anno_file_content, dataset_list, file_id, file_name)

        # save old versions
        #todo: USERs choise to save old versions
        gitcall = githandling.GitCall(hist_path)
        entry_paths = get_file_paths(hist_path, 'html')
        temp_path = os.path.join(hist_path,"temp")
        old_versions=[]
        dropdown_options=[]
        try:                    
            # Creats a new dir "temp" and clones the repository into it.
            os.mkdir(temp_path)  #  Creat a temporary directory
            gitcall.clone(temp_path).communicate()    
            gitcall_temp = githandling.GitCall(temp_path)        
             
            for entry_path in entry_paths:
                commit_date, commit_hash = githandling.commit_msg_readout(gitcall, entry_path)
                if commit_date:
                    temp_file_path = os.path.join(temp_path,os.path.basename(entry_path))
                    old_ver_html_content, old_ver_rst_content = githandling.checkout_old_version(gitcall_temp, commit_hash, temp_file_path)
                                
                    # Validates the seleced file for a non commited change and appends it to the date and content list.
                    commit_date, old_ver_html_content, old_ver_rst_content = githandling.check_current_working_file(commit_date, old_ver_html_content, old_ver_rst_content, entry_path)
                                
                    # annolink update for old versions
                    updated_old_ver = []
                    for ver in old_ver_html_content:
                        update_content = update_annolink(ver, dataset_list, 'hist')                   
                        updated_old_ver.append(update_content)
                                
                    # Create an insertable option tag for the js dropdown menus containing the commit dates.
                    dropdown_option_tag = eln_util.list_to_jsoption_tag(range(0,len(commit_date)), commit_date)
                    
                    old_versions.append(updated_old_ver)
                    dropdown_options.append(dropdown_option_tag)
                else:
                    old_versions.append('')
                    dropdown_options.append('')
                    
            # Convert old_versions to json object
            old_versions_dict = {}
            for i, entries_list in enumerate(old_versions):
                old_versions_dict[i]= entries_list
            old_versions_json = json.dumps(old_versions_dict)
            
            # Load old versions for annotations
            eln_dir_path = eln_util.eln_path()  
            anno_css_path = os.path.join(eln_dir_path,'static','css','anno_export.css')
            with open(anno_css_path,'r') as anno_css:
                anno_export_css = anno_css.read()
            
            for dataset in dataset_list:
                if dataset.has_annotation():                     

                    anno_path = os.path.join(hist_path,'annotations', dataset.anno_file_html)
                    with open(anno_path,'r') as anno_file:
                        file_content = anno_file.read()
                    header, content =file_content.split('\n', 1)
                    annotation =  experiment.Annotation(header, content)                   
                    
                    commit_date, commit_hash = githandling.commit_msg_readout(gitcall, anno_path)
                    old_ver_json = ''
                    dropdown_option_tag = ''
                    if commit_date:
                        temp_anno_path = os.path.join(temp_path,'annotations',os.path.basename(anno_path))
                        old_ver_html_content, old_ver_rst_content = githandling.checkout_old_version(gitcall_temp, commit_hash, temp_anno_path)                        
                        commit_date, old_ver_html_content, old_ver_rst_content = githandling.check_current_working_file(commit_date, old_ver_html_content, old_ver_rst_content, anno_path)                
                        dropdown_option_tag = eln_util.list_to_jsoption_tag(range(0,len(commit_date)), commit_date)            
                        
                        # annolink update for old versions
                        updated_anno_old_ver = []
                        for ver in old_ver_html_content:
                            update_content = update_annolink(ver, dataset_list, 'anno')                   
                            updated_anno_old_ver.append(update_content)

                        old_ver_json = json.dumps(updated_anno_old_ver)


                    anno_page_content = update_annolink(dataset.anno_content, dataset_list, 'anno')                            
                    anno_entry = generate_entry_html (annotation, anno_page_content, dropdown_option_tag)
                    anno_page = generate_anno_html(anno_entry,
                                                   old_ver_json,
                                                   annotation,
                                                   anno_export_css,
                                                   )
                    
                    zf.writestr(dataset.anno_export_path, anno_page.encode('utf-8'))
        finally:
            if os.path.isdir(temp_path):
                shutil.rmtree(temp_path)


        # save notebook.html
        file_org = {}
        file_org = sorted(experiment.get_sections(hist_path),
                          key=lambda x: x.section_no)
        entries_html = ''        
        for i, entry in enumerate(file_org):
            content = update_annolink(entry.content, dataset_list, 'hist')
            entries_html +=  generate_entry_html (entry, content, dropdown_options[i], str(i))
        
        eln_dir_path = eln_util.eln_path()    
        css_path = os.path.join(eln_dir_path,'static','css','export.css')
        with open(css_path,'r') as css:
            export_css = css.read()
                
        dataset_legend_html = generate_dataset_legend(dataset_list)
        
        
        notebook_page = notebook_template_html(hist_name, export_css, entries_html, old_versions_json, dataset_legend_html)
        zf.writestr('notebook.html', notebook_page.encode('utf-8'))
    finally:
        zf.close()
    
    return



        ##
        # test = trans.security.decode_id(enc_hist_id)
        # with open('/home/hess/Desktop/test_log.txt', 'w') as file:
        #     file.write(db_modules.get_hist_name(hist_id))
        ##





