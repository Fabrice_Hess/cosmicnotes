#!/usr/bin/env python3.4

import os
import sys
import urllib2
import sqlalchemy
import json
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.sql import and_, or_, not_

import eln_util

def change_settings(input):
    """ Manipulates the database depending on the received input.
    """
    # parse input of the mako input
    hist_id, operation_var, operation = input.split("|")
    
    
    # Connection to database
    engine = create_engine(eln_util.ElnConfig().db_conn())
    connection = engine.connect()
    metadata = MetaData()
    metadata.bind=engine
    
    # Load Tables
    projects_assign = Table('projects_assign', metadata, autoload=True)
    projects = Table('projects', metadata, autoload=True)
    histories = Table('histories', metadata, autoload=True)
    
    
    if operation == "Ins":
        # Inserts a new history-project assignment to the db.
        project_id = operation_var
        connection.execute(projects_assign.
                        insert().
                        values(
                                project_id = project_id,
                                hist_id = hist_id
                                )
                        )
    

    elif operation == "Del":
        # Delets a history-project assignment out of the db.
        project_id = operation_var
        connection.execute(projects_assign.
                        delete().
                        where(
                                and_(
                                    projects_assign.c.hist_id == hist_id, 
                                    projects_assign.c.project_id == project_id,
                                    )
                             ) 
                        )
    
    elif operation == "New":
         # Creats a new project in the db and a new assignment to that project.
        project_name = operation_var
        result = connection.execute(projects.
                                    insert().
                                    values(project_name = project_name)
                                )
        project_id = result.inserted_primary_key[0]
        
        connection.execute(projects_assign.
                        insert().
                        values(
                                project_id = project_id,
                                hist_id = hist_id
                                )
                        )
                        
    elif operation == "Name":
        # Update the history name in the "histories" table.
        #TODO: The name in the db gets overwritten.
        hist_name = operation_var
        connection.execute(histories.
                        update().
                        where(histories.c.hist_id == hist_id).
                        values(hist_name=hist_name)
                        )    


def create_histdict (trans, search_author, search_project, search_text, sub_filter):
    """ Creates a dictionary, containing necessary history information for the multi-
    panel view. The database query searches for histories restricted by author and project
    assignment. Afterwards the dictionary gets filtered by the submitted status. 
    """
    # Connection to database
    engine = create_engine(eln_util.ElnConfig().db_conn())
    connection = engine.connect()
    metadata = MetaData()
    metadata.bind = engine
    
    # Load Tables
    histories = Table('histories', metadata, autoload=True)
    annotations = Table('annotations', metadata, autoload=True)
    projects = Table('projects', metadata, autoload=True)
    projects_assign = Table('projects_assign', metadata, autoload=True)
    entries = Table('entries', metadata, autoload=True)
    
    # Check database for all history ids and history names.
    histdicts = []
    
    if (search_author == '') and (search_project == ''):
        result = connection.execute(histories.select())
    
    elif (search_author != '') and (search_project == ''):
        result = connection.execute(histories.
                                    select().
                                    where(histories.c.user_name == search_author)
                                )
    
    elif (search_author == '') and (search_project != ''):
        result_id = connection.execute(projects.select().
                                    where(projects.c.project_name == search_project))
        project_id = result_id.fetchone()
        
        result_assign = connection.execute(projects_assign.select().
                                        where(projects_assign.c.project_id == project_id['project_id']))
        hist_id_list= []
        for row in result_assign:
            hist_id_list.append(row['hist_id'])
        
        result = connection.execute(histories.
                                    select().
                                    where(histories.c.hist_id.in_(hist_id_list))
                                )
    
    elif (search_author != '') and (search_project != ''):
        result_id = connection.execute(projects.select().
                                    where(projects.c.project_name == search_project))
        project_id = result_id.fetchone()
        
        result_assign = connection.execute(projects_assign.select().
                                    where(projects_assign.c.project_id == project_id['project_id']))
        hist_id_list= []
        for row in result_assign:
            hist_id_list.append(row['hist_id'])
            
        result = connection.execute(histories.
                                    select().
                                    where(
                                        and_(
                                                histories.c.hist_id.in_(hist_id_list),
                                                histories.c.user_name == search_author,
                                            )
                                        )
                                )   
    
    for row in result:
        hist  = { 'id': trans.security.encode_id( row[0] ),
                'name' : str(row[1]),
                'user_name' :  eln_util.fit_datetime_format(row[2]),
                'create_time': eln_util.fit_datetime_format(row[4]),
                'update_time': eln_util.fit_datetime_format(row[5]),
                'model_class': 'History', 
                'submitted': str(row[6])}
        histdicts.append(hist)
    
    
    # Text search
    # Search in lab book
    lb_search_hits = []
    anno_search_hits = []   
    hist_search_hits =[]
    if search_text != '':
        s = entries.select(entries.c.entry_content.match(search_text))
        result = connection.execute(s)
        
        for row in result:
            lb_search_hits.append(row['hist_id'])
            hist_search_hits.append(row['hist_id'])
        
        # Search in annotations
        s = annotations.select(annotations.c.anno_content.match(search_text))
        result = connection.execute(s)
 
        for row in result:
            anno_search_hits.append(row['anno_id'])
            hist_search_hits.append(row['hist_id']) 
    
        # Filter text search
        filter_histdicts = []
        for hist in histdicts:
            test_hist_id = int(trans.security.decode_id( hist['id'] ))
    
            if test_hist_id in hist_search_hits:
                filter_histdicts.append(hist)
        histdicts = filter_histdicts
 
    # Filter histdicts for submitted elns.
    if sub_filter == 'both':
        pass
        
    elif sub_filter == 'only_submitted':
        filter_histdicts = []
        for hist in histdicts:
            if hist['submitted'] == 'y':
                filter_histdicts.append(hist)
        histdicts = filter_histdicts
            
    elif sub_filter == 'only_non_submitted':
        filter_histdicts = []
        for hist in histdicts:
            if hist['submitted'] == 'n':
                filter_histdicts.append(hist)
        histdicts = filter_histdicts
            
    return histdicts, lb_search_hits, anno_search_hits

def create_tables():
    """ Creates necessary tables in the db, if they are not already existing.
    """
    engine = create_engine(eln_util.ElnConfig().db_conn())
    connection = engine.connect()
    
    metadata = MetaData()    

    histories = Table('histories', metadata,
        Column('hist_id', Integer, primary_key=True),
        Column('hist_name', String(200)),
        Column('user_name', String(100)),
        Column('hist_path', String(200)),
        Column('creation_time', String(20)),
        Column('update_time', String(20)),
        Column('submitted', String(1)),
    )
    
    projects = Table('projects', metadata,
        Column('project_id', Integer, primary_key=True),
        Column('project_name', String(500)),
    )
    
    projects_assign = Table('projects_assign', metadata,
        Column('project_assign_id', Integer, primary_key=True),
        Column('project_id', Integer),
        Column('hist_id', Integer),
    )  
        
    if eln_util.ElnConfig().db_is_mysql():
        annotations = Table('annotations', metadata,
            Column('anno_id', Integer, primary_key=True),
            Column('hist_id', Integer),
            Column('anno_path', String(200)),
            Column('anno_content',String(21000)),
            mysql_engine='MyISAM',
        )
        
        entries = Table('entries', metadata,
            Column('entry_id', Integer, primary_key=True),
            Column('hist_id', Integer),
            Column('entry_no', Integer),
            Column('entry_content', String(21000)), 
            mysql_engine='MyISAM',       
        )
    
    else:
        annotations = Table('annotations', metadata,
            Column('anno_id', Integer, primary_key=True),
            Column('hist_id', Integer),
            Column('anno_path', String(200)),
            Column('anno_content',String(21000)),
        )
        
        entries = Table('entries', metadata,
            Column('entry_id', Integer, primary_key=True),
            Column('hist_id', Integer),
            Column('entry_no', Integer),
            Column('entry_content', String(21000)), 
        )
      
    metadata.create_all(engine)
    return

def get_project_list_of_hist(hist_id):
    """ Returns a list of all project names, that are assigned to the received hist id. 
    """
    # Connection to database
    engine = create_engine(eln_util.ElnConfig().db_conn())
    connection = engine.connect()
    metadata = MetaData()
    metadata.bind=engine
        
    # Load Tables
    projects_assign = Table('projects_assign', metadata, autoload=True)
    projects = Table('projects', metadata, autoload=True)

    # Get project_ids of this history from the database.
    result = connection.execute(projects_assign
                                .select()
                                .where(projects_assign.c.hist_id == hist_id))
    project_ids = []
    for row in result:
        project_ids.append(row[projects_assign.c.project_id])
    
    # Get project_names via the ids from the database.
    result = connection.execute(projects.select()
                                        .where(projects.c.project_id.in_(project_ids)))
    project_names_list = []
    for row in result:
        project_names_list.append(row[projects.c.project_name])
        
    return project_names_list
    
    
    
def get_hist_name (hist_id):
    # Connection to database
    engine = create_engine(eln_util.ElnConfig().db_conn())
    connection = engine.connect()
    metadata = MetaData()
    metadata.bind = engine
    
    # Load Tables
    histories = Table('histories', metadata, autoload=True)
    
    s = histories.select().where(histories.c.hist_id == hist_id)
    result = connection.execute(s)
    out = result.fetchone()
    return out['hist_name']
    
    
def annolink_other_histories (hist_id, user, root, trans):
    
    # Connection to database
    engine = create_engine(eln_util.ElnConfig().db_conn())
    connection = engine.connect()
    metadata = MetaData()
    metadata.bind = engine
    
    # Load Tables
    projects_assign = Table('projects_assign', metadata, autoload=True)
    histories = Table('histories', metadata, autoload=True)
    annotations = Table('annotations', metadata, autoload=True)

        # get all projects of this History
    s = projects_assign.select().where(projects_assign.c.hist_id == hist_id)
    result = connection.execute(s)
    project_ids = []
    for row in result:
        project_ids.append(row[projects_assign.c.project_id])

        # get all histories of the projects
    s = projects_assign.select().where(projects_assign.c.project_id.in_(project_ids))        
    result = connection.execute(s)
    all_hist_ids = []
    for row in result:
        all_hist_ids.append(row[projects_assign.c.hist_id])

        # filter for user and get history names
    s = histories.select().where(
                                 and_(
                                      histories.c.hist_id.in_(all_hist_ids),
                                      histories.c.user_name == user
                                      )             
                                )        
    result = connection.execute(s)
    hist_dict = {}
    for row in result:
        hist_dict[row[histories.c.hist_id]] = row[histories.c.hist_name]
        
        #check db for annos
    anno_dict = {}
    for hist_id in hist_dict:
        s = annotations.select().where(annotations.c.hist_id == hist_id)
        result = connection.execute(s)        
        anno_list = []
        for row in result:
            anno_list.append(int(row[annotations.c.anno_id]))
        anno_dict[hist_id] = anno_list
        
        
        #galaxy request per history to get names
    content_list = []
    anno_name_url_dict = {}
    for hist_id in anno_dict:
        enc_hist_id = str(trans.security.encode_id(hist_id))
        url = root+'api/histories/'+enc_hist_id+'/contents'; 
        headers = {'Cookie' : 'galaxysession='+trans.get_cookie()}        
        request = urllib2.Request(url, headers = headers)    
        response = urllib2.urlopen(request)
        history_content_dict = json.loads(response.read())

        for dataset in history_content_dict:
            if trans.security.decode_id(dataset['id']) in anno_dict[hist_id]:
                
                anno_url = root+'visualization/show/annotation?dataset_id='+dataset['id']  
                anno_name_url_dict[trans.security.decode_id(dataset['id'])] = [dataset['name'], anno_url]
            
            
    # contruct response json object
    respond_dict = {}    
    for hist_id in hist_dict:
        response_anno_dict ={}
        for anno_id in anno_dict[hist_id]:
            response_anno_dict[anno_id] = anno_name_url_dict[anno_id]            
        respond_dict[hist_id] = [response_anno_dict, hist_dict[hist_id]]        

    json_response = json.dumps(respond_dict)    
    return json_response
         