#!/usr/bin/env python2.7

from datetime import datetime
import os
import pypandoc 

from mako.template import Template

def timestamp ():
    datetime_str = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    return datetime.strptime(datetime_str,"%Y-%m-%d %H:%M:%S")

class Section (object):
    def __init__ (self, header = None, content = ''):
        if header is None:
            self.history_name = ''
            self.user_name = ''
            self.user_email = ''
            self.history_id = ''
            current_date = timestamp()
            self.date_created = current_date
            self.date_last_modified = current_date
            self.section_no = 1
            self.erratum_no = 0
            self.is_erratum = False
        else:
            meta_fields = header.split('|')
            self.history_name = meta_fields[0]
            self.user_name = meta_fields[1]
            self.user_email = meta_fields[2]
            self.history_id = meta_fields[3]
            self.date_created = datetime.strptime(meta_fields[4],
                                                  "%Y-%m-%d %H:%M:%S")
            self.date_last_modified = datetime.strptime(meta_fields[5],
                                                        "%Y-%m-%d %H:%M:%S")
            self.erratum_no = int(meta_fields[6])
            self.is_erratum = False
            if self.erratum_no > 0:
                self.is_erratum = True
            self.section_no = int(meta_fields[7])
            
            if len(meta_fields) > 8:
                # Call in cgi script, header contains two additional entrys. 
                self.encoded_dataset_id = meta_fields[8]
                self.input_tag = meta_fields[9]
        self.content = content
        
    def is_modified (self):
        return self.date_created.date() != self.date_last_modified.date()
        
    def create_file_header (self):
        return "|".join([self.history_name, self.user_name,
                         self.user_email, self.history_id,
                         str(self.date_created), str(self.date_last_modified),
                         str(self.erratum_no), str(self.section_no)])
        
def get_sections (exp_dir):
    if os.path.exists(exp_dir):
        # Check the current history_path for html files.
        # Parse file content and headers into list.
        for file in os.listdir(exp_dir):
            if file.endswith(".html"):
                file_path = os.path.join(exp_dir, str(file)) 
                saved_file = Template(filename=file_path, 
                                      input_encoding='utf-8',
                                      output_encoding='utf-8')
                file_content = saved_file.render_unicode()

                header, file_txt = file_content.split('\n', 1)
                # CKEditor adds \r\n to the end of documents,
                # which would cause an extra newline in the rendered mako code!
                file_txt.replace("\n", "").replace("\r","")                
                section = Section(header, file_txt)
                yield section
                

class Annotation (object):
    def __init__ (self, header = None, content = ''):
       
        if header is None:
            self.history_name = ''
            self.dataset_name = ''
            self.user_name = ''
            self.user_email = ''
            current_date = timestamp()
            self.date_created = current_date
            self.date_last_modified = current_date
            self.history_id = ''
            self.dataset_number = ''
            self.dataset_id = ''
        
        else:
            meta_fields = header.split('|')
            self.history_name = meta_fields[0]
            self.dataset_name = meta_fields[1]
            self.user_name = meta_fields[2]
            self.user_email = meta_fields[3]
            self.date_created = datetime.strptime(meta_fields[4],
                                                "%Y-%m-%d %H:%M:%S")
            self.date_last_modified = datetime.strptime(meta_fields[5],
                                                        "%Y-%m-%d %H:%M:%S")
            self.history_id = meta_fields[6]
            self.dataset_number = meta_fields[7]
            self.dataset_id = meta_fields[8]
            if len(meta_fields) > 9:
                # Call in save.py script, header contains two additional entrys. 
                self.encoded_dataset_id = meta_fields[9]
                self.input_tag = meta_fields[10]

        self.content = content

    def is_modified (self):
        return self.date_created.date() != self.date_last_modified.date()

    def create_file_header (self):
        return "|".join([self.history_name, self.dataset_name,
                         self.user_name, self.user_email, 
                         str(self.date_created), str(self.date_last_modified),
                         self.history_id, self.dataset_number, self.dataset_id])

def get_annotations (anno_dir):
    """ Checks for every annotation in the received directory and create for each an 
    annotation object.
    """
    if os.path.exists(anno_dir):
        for file in os.listdir(anno_dir):                                                                  
            if file.endswith(".html"): 
                file_path = os.path.join(anno_dir, str(file))
                saved_file = Template(filename=file_path,
                                      input_encoding='utf-8',
                                      output_encoding='utf-8')
                file_content = saved_file.render_unicode()
                header, content = file_content.split('\n', 1)
                anno = Annotation(header, content)
                yield anno
            

