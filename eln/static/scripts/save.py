#!/usr/bin/env python3.4

import pypandoc               # wrapper for pandoc, enables easy transformation of txt formats
import os
import datetime
import sys
import codecs
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.sql import and_, or_, not_

import githandling
import db_modules
import eln_util
import experiment 

def save(header, editor_txt):
    
    # Get archive path via dir structure.
    archive_path = eln_util.archive_path()
        
    # parse input, dependend of the mako input
    header_split = header.split("|")
    
    if header_split[-1] in ["labbook_entry","terminal_submit"]:
        input = experiment.Section(header, editor_txt)
        
    elif header_split[-1] == "annotation":
        input = experiment.Annotation(header, editor_txt)
    
    # header processing, change last modify date, cut off encoded dataset_id and the data_tag
    current_datetime_out = experiment.timestamp()
    input.date_last_modified = str(current_datetime_out)
    header_out = input.create_file_header()
    
    # Connection to database
    engine = create_engine(eln_util.ElnConfig().db_conn())
    connection = engine.connect()
    metadata = MetaData()
    metadata.bind=engine
    
    # Update the history name in the "histories" table
    histories = Table('histories', metadata, autoload=True)
    entries = Table('entries', metadata, autoload=True)
    annotations = Table('annotations', metadata, autoload=True)
    
    connection.execute(histories.
                        update().
                        where(histories.c.hist_id == input.history_id).
                        values(hist_name=input.history_name)
                        )
    
    # Check database table 'histories' and get the hist_path. 
    result = connection.execute(histories.
                                select().
                                where(histories.c.hist_id == input.history_id)
                                )
    out = result.fetchone()
    
    hist_path_tail = out[histories.c.hist_path]
    hist_path = os.path.join(archive_path, hist_path_tail)
    anno_path = os.path.join(hist_path,'annotations')
    
    if input.input_tag == "annotation":
        annotations = Table('annotations', metadata, autoload=True)
        result = connection.execute(annotations.
                                    select().
                                    where(annotations.c.anno_id == input.dataset_id)
                                    )
        out = result.fetchone()
        if not out:
            connection.execute(annotations.
                                insert().
                                values(anno_id=input.dataset_id,
                                        hist_id=input.history_id, 
                                        anno_path=os.path.join('annotations',input.dataset_id))
                                )
    anno_path = os.path.join(hist_path,'annotations')    
    
    gitcall = githandling.GitCall(hist_path)
    
    if input.input_tag != "terminal_submit":
        commit_now = False          # init commit_now flag 
    
        # make new directories, initialize a new git repository and configure git.
        if not os.path.isdir(hist_path):
            os.mkdir(hist_path)
            #os.chmod(hist_path,0o777)
            githandling.create_commit_next(hist_path)
            
            gitcall.init(hist_path).communicate()
            gitcall.config("user.name", input.user_name).communicate()
            gitcall.config("user.email", input.user_email).communicate()
            
        if input.input_tag == "annotation":
            if not os.path.isdir(anno_path):
                os.mkdir(anno_path)
                #os.chmod(anno_path,0o777)
                
                
        # Checks every txt file in the hist_dir or anno_dir for changes and checks if one
        # day had passed since the last commit. If the conditions are ok, git adds the 
        # file and the commit_now flagg is set to True.        
        stdout_last_date, stderr = gitcall.log_pretty().communicate()
        for file in os.listdir(hist_path):
            if file.endswith(".txt"):         
                file_path = os.path.join(hist_path,file)
                if githandling.validate_diff (gitcall, file_path):
                    git_last_com_date_obj = datetime.datetime.strptime(stdout_last_date[:-6],"%Y-%m-%d %H:%M:%S")
                    if githandling.validate_date(file_path, git_last_com_date_obj):
                        gitcall.add(file_path).communicate()
                        last_modify_date_obj = githandling.get_file_date(file_path)
                        commit_now = True
    
        if os.path.isdir(anno_path):
            for file in os.listdir(anno_path):
                if file.endswith(".txt"):
                    file_path = os.path.join(anno_path,file)
                    if githandling.validate_diff (gitcall, file_path):
                        git_last_com_date_obj = datetime.datetime.strptime(stdout_last_date[:-6],"%Y-%m-%d %H:%M:%S")
                        if githandling.validate_date(file_path,git_last_com_date_obj):
                            gitcall.add(file_path).communicate()
                            last_modify_date_obj = githandling.get_file_date(file_path)
                            commit_now = True
    
        # Read the .commit_next file. When its creation date is not today, Git add every entry of the file.
        # Replace the existing commit_now file with a new one and set the commit_now flagg to True.
        if os.path.isfile(os.path.join(hist_path,".commit_next")):
            last_modify_date_obj_commit_next, entry_list = githandling.read_commit_next(hist_path)
            if entry_list:
                if datetime.datetime.date(last_modify_date_obj_commit_next) != datetime.datetime.date(current_datetime_out):   
                    last_modify_date_obj = last_modify_date_obj_commit_next
                    for filename in entry_list:
                        gitcall.add(filename).communicate()
                        githandling.create_commit_next(hist_path)
                        commit_now = True   
        
        # When the mako input is a labbook entry, add to commit_next if the file is not existing yet. When the input is an erratum add/commit.
        # When the mako input is an annotation, add to commit_next.
        # Also set up under both conditions he file_names and the url extensions for the output.
        if input.input_tag == "labbook_entry":
            back_url = "eln"
            saved_filename = "part_"+str(input.section_no)+".html"
            if input.erratum_no == 0:     # when the saved part isn't a erratum add to .commit_next
    
                githandling.add_to_commit_next(hist_path, saved_filename)
                
            else:
                # When a erratum part was saved, perform a git add/commit.
                gitcall.add('part_'+str(input.section_no)+'.html').communicate()
                commit_message = githandling.message_creator(gitcall, current_datetime_out)
                gitcall.commit("Erratum added\n"+commit_message).communicate() 
    
        elif input.input_tag == "annotation":
            back_url ="annotation"
            saved_filename= os.path.join("annotations",str(input.dataset_id)+".html")
            githandling.add_to_commit_next(hist_path,saved_filename)           
    
        # When commit_now flagg was set up, create a commit_message and perform a git commit.
        if commit_now:
            commit_message = githandling.message_creator(gitcall ,last_modify_date_obj)
            gitcall.commit(commit_message).communicate()
    
        # format convertion to plain text
        editor_txt_plain = pypandoc.convert(input.content, 'plain', format = 'html')
           

        # Load existing file content        
        file_content = ""
        save_path = os.path.join(hist_path,saved_filename)
        if os.path.isfile(save_path):
            with codecs.open(save_path,'r',encoding='utf8') as file:
                old_file_content = file.read()
                header, file_content = old_file_content.split('\n', 1) # Cut off header 
    
        # set up conditions when file should be saved
        need_save = True
        if os.path.isfile(save_path):
            if file_content == input.content:
                need_save = False
        elif editor_txt_plain.isspace():
            need_save = False
            
        # write files, html file for reloading into the editor, txt file for storage and version control
        if need_save:    # when file is not existing or old content and new content dont match
            
            with codecs.open(save_path,'w',encoding='utf8') as file_html:
                file_html.write(header_out+"\n"+input.content)
           
            # splice editor txt to max 21000 characters. Thats the limit of mysql row content with utf-8 encoding.
            editor_txt_plain = editor_txt_plain[:21000]
            
            # Update/Create db entry with content.
            if input.input_tag == "labbook_entry": 
                # Check database for the hist_id and get the path.
                s = entries.select().where(and_( entries.c.hist_id == input.history_id,
                                                entries.c.entry_no == input.section_no,
                                            )
                                        )            
                result = connection.execute(s)
                out = result.fetchone()
                
                if out:
                    connection.execute(entries.
                                    update().
                                    where(entries.c.entry_id == out['entry_id']).
                                    values(entry_content = editor_txt_plain)
                                    )
        
                elif eln_util.ElnConfig().db_is_postgres() or eln_util.ElnConfig().db_is_mysql() :
                    connection.execute(entries.
                                    insert().
                                    values(hist_id = input.history_id,
                                            entry_no = input.section_no,
                                            entry_content = editor_txt_plain,
                                            )
                                    )
            
            elif input.input_tag == "annotation": 
                if eln_util.ElnConfig().db_is_postgres() or eln_util.ElnConfig().db_is_mysql():
                    connection.execute(annotations.
                                    update().
                                    where(annotations.c.anno_id == input.dataset_id).
                                    values(anno_content = editor_txt_plain)
                                    )
            
            # Update database, set update_time to current time. 
            connection.execute(histories.
                            update().
                            where(histories.c.hist_id == input.history_id).
                            values(update_time = current_datetime_out)
                            )
            
            # Set up the output messege dependend of the input data.
            if input.input_tag == "labbook_entry":  
                out_message ="Your notebook entry has been successfully saved."                      
            elif input.input_tag == "annotation":           
                out_message ="Your annotation has been successfully saved."
            
            
    # data_tag: terminal_submit initiates the terminally submit -> creats .lock file.
    # Add/commits every txt file in the history and the lock-file 
    else:
        lock_file = open(os.path.join(hist_path,'.lock'), 'w')
        lock_file.write(header_out)
        lock_file.close()
        gitcall.add(".lock").communicate()
        
        for file in os.listdir(hist_path):
            if file.endswith(".html"):
                file_path = os.path.join(hist_path,file)
                gitcall.add(file_path).communicate()
    
        if os.path.isdir(anno_path):
            for file in os.listdir(anno_path):
                if file.endswith(".html"):
                    file_path = os.path.join(anno_path,file)
                    gitcall.add(file_path).communicate()      
        
        commit_message = githandling.message_creator(gitcall ,current_datetime_out)
        gitcall.commit(commit_message+"\nTerminal Submit").communicate()
        os.remove(os.path.join(hist_path,".commit_next")) # remove the commit next file. 
        back_url ="eln"
        out_message = "Your project has been successfully terminal submitted."
        # Update database, set submitted flagg to 'y'.
        connection.execute(histories.
                        update().
                        where(histories.c.hist_id == input.history_id).
                        values(submitted='y')
                        )
    return

