#!/usr/bin/env python3.4

import json
import os
import ConfigParser

class ElnConfig (ConfigParser.RawConfigParser):
    def __init__(self):
        ConfigParser.RawConfigParser.__init__(self)
        self.read(config_file_path())

    def export_enabled(self):
        if not self.has_option('Export', 'enable_export'):
            raise LookupError ('enable_export is not configurated')
        return self.getboolean('Export', 'enable_export')    
        
    def get_export_path(self):
        if not self.has_option('Export', 'export_path'):
            raise LookupError ('export_path is not configurated')
        path = self.get('Export', 'export_path')
        if os.path.isdir(path):
            return path
        else:
            raise IOError ('Not a valid directory:'+path)
            
    def get_autosave_interval (self):
        if not self.has_option('Editor', 'autosave_timer'):
            raise LookupError(('autosave_timer is not configurated'))
        interval = self.get('Editor', 'autosave_timer')
        try:
            int_interval = int(interval)
        except:
            raise ValueError ('autosave_timer has to be a natural number')
        return interval
        
    def db_conn (self):
        #TODO: handle exceptions
        if not self.has_option('Database', 'database_connection_string'):
            raise LookupError(('database_connection_string is not configurated'))
        return self.get('Database', 'database_connection_string')
        
    def db_is_postgres(self):
        db_string = self.db_conn()
        return db_string.startswith('postgres')
        
    def db_is_mysql(self):
        db_string = self.db_conn()
        return db_string.startswith('mysql')

class GalaxyConfig (ConfigParser.RawConfigParser):
    #TODO: hardcoded path
    #TODO: handle default sting + unaccepted db
    def __init__(self):
        ConfigParser.RawConfigParser.__init__(self)
        eln_dir_path = eln_path()
        visu_dir_path = os.path.dirname(eln_dir_path)
        plugin_path = os.path.dirname(visu_dir_path)
        galaxy_config_path = os.path.dirname(plugin_path)
        galaxy_ini_path = os.path.join(galaxy_config_path,'galaxy.ini')
        if not os.path.isfile(galaxy_ini_path):
            galaxy_ini_path = os.path.join(galaxy_config_path,'galaxy.ini.sample')
        
        self.read(galaxy_ini_path)
    
    def get_db_string(self):
        if not self.has_option('app:main', 'database_connection'):
            raise LookupError ('database_connection is not configurated')
        return self.get('app:main', 'database_connection')

def create_json_string (anno_list, trans, root):
    """Creates a Json string with the dataset number, name and url out of a list of
    annotation objects. The Json string can be pasered by Javascript and the informations
    are used for the anno_link button.
    """
    annolink_data = {}
    dataset_name_list = []
    dataset_number_list = []
    dataset_url_list = []
    
    for anno in anno_list:
        dataset_number_list.append(anno.dataset_number) 
        dataset_name_list.append(anno.dataset_name)
        dataset_url_list.append(root+'visualization/show/annotation?dataset_id='+trans.security.encode_id(anno.dataset_id))      
                  
    annolink_data['dataset_number'] = dataset_number_list
    annolink_data['dataset_name'] = dataset_name_list
    annolink_data['dataset_url'] = dataset_url_list          
    
    return json.dumps(annolink_data)

def fit_datetime_format (datetime_obj):
    """ Adjusts the date format, for the input dictionary of galaxys view multiple.
    """
    date = str(datetime_obj)
    return date.replace(' ', 'T')
    
    
def list_to_jsoption_tag (list_val, list_cont):
    """Formates a list to a js option string. eg. for a dropdown menu
    First list fills the values and the second list the content of the option.
    input: list.
    output: string.
    """
    if len(list_val) != len(list_cont):
        raise ValueError ('List of values and list of content have to have the same lenght.')
    dropdown_option_tag = ""
    for i in range(0,len(list_cont)):
        # eg. <option value="0">2000-12-12</option>
        dropdown_option_tag += "<option value='"+str(list_val[i])+"'>"+list_cont[i]+"</option>"   
    return dropdown_option_tag
    
    
def list_to_jsarray(list):
    """Convert a list into a string that can be read as a javascript array.
    The string have to be inserted in a array declaration in js code.
    input: list; html content of old versions.
    output: string; html content of old versions.
    """
    old_content_str = ""
    for entry in list:
        # Double backslash is necessary because javascript uses backslash also as escape.
        entry = entry.replace("\\","\\\\")      # escape user input "\"
        entry = entry.replace("'","\\'")        # escape user input "'", so javascript creats a correct array
        old_content_str += "'"+entry+"',"
    old_content_str = old_content_str.rstrip(",")   # eg. 'Version content1','Version content2','Version content3'
    return old_content_str

def archive_path ():
    """ Returns the path of the eln archive on the system.
    Dependend of the location of this script. 
    """
    script_path = os.path.dirname(os.path.realpath(__file__))
    script_dir_path = os.path.dirname(script_path)
    static_dir_path = os.path.dirname(script_dir_path)
    visu_dir_path = os.path.dirname(static_dir_path)
    plugins_dir_path = os.path.dirname(visu_dir_path) 
    config_dir_path = os.path.dirname(plugins_dir_path)
    galaxy_root_path = os.path.dirname(config_dir_path)
    archive_path =  os.path.join(galaxy_root_path,'database','files','eln_files')
    if not os.path.isdir(archive_path):
        os.mkdir(archive_path)
    return archive_path
    
def eln_path ():
    script_path = os.path.dirname(os.path.realpath(__file__))
    script_dir_path = os.path.dirname(script_path)
    static_dir_path = os.path.dirname(script_dir_path)
    visu_dir_path = os.path.dirname(static_dir_path)        
    eln_path = os.path.join(visu_dir_path,'eln')
    return eln_path  
    
def config_file_path():
    """ Returns the path of the config dir on the system.
    """
    eln_dir_pah = eln_path()
    config_file_path = os.path.join(eln_dir_pah, 'config', 'eln.cfg')
    return config_file_path