#!/usr/bin/env python3.4
import subprocess
import os
import datetime
import sys
import codecs
import pypandoc

import experiment

class GitCall (object):
    """ This class eneables git commandline calls via a subprocess.Popen object.
    While initializing an instance, the path of the repository have to be passed.
    The output of all methods are Popen objects. To interact with them you have to 
    use the communicate() method of the subprocess modul.
    """
    
    def __init__ (self, cwd):
        """ While initializing a new class object. The path of the repository have to be 
        given. 
        """
        self.cwd = cwd        

        
    def diff_name_only (self, file_path):
        """ Executes a git diff in the current repository.
        When there are diffs in the repostory, the output are only the file names of the
        changed files.
        """
        return self.call(["diff","--name-only", file_path])


    def commit (self, commit_message):
        """ Executes a git commit with the given commit message in the current repository.
        """
        return self.call(["commit", "-m", commit_message])


    def status_porcelain (self):
        """ Outputs the repository status in the porcelein format.
        eg. "M  example.txt
             A  example2.txt"
        """
        return self.call(["status","--porcelain"])


    def log_pretty (self, file_path = None):
        """ Lists the commits of the current repository of a specific file.
        When no file path is given it outputs only the date of the latest commit in the 
        whole repository.
        The Output is formatted like: first_line(date)#commit_hash.
        """
        # %s: subject, first line of the commit message. #: Seperator. %H: commit hash.
        if file_path:
            return self.call(["log","--pretty=format:%s#%H",file_path])
        else:
            return self.call(["log","--pretty=format:%ci","-n","1"])

    def checkout (self, commit_hash, file_path):
        """ Restores the file to the recieved commit state.
        """
        return self.call(["checkout", commit_hash, file_path])


    def clone (self, clone_target_path):
        """ Clones the current repository to the target path.
        When the target_path doesn't exits, git creats the directory.
        """
        return self.call(["clone", ".", clone_target_path])    


    def add (self, file_path):
        """ Adds the received file to git repository HEAD.
        """
        return self.call(["add",file_path])       


    def config (self, config_type, entry):
        """ Changes the configuration of the git repository.
        The first argument is the configuration type, which should be changed.
        The second argument is the actual configuration value, which is used to replace 
        the current configuration.
        """
        return self.call(["config", config_type, entry])  

               
    def init (self, path):
        """ Initializes a new git repository into the received path.
        """
        return self.call(["init", path])  

        
    def call (self, *args, **kwargs):
        """ Creats a Popen object via a subprocess.Popen call.
        Changes the cwd to the class given cwd and restores the original cwd after 
        generating the Popen object. Additionally to the received arguments via the
        method call, some arguments and keyword arguments are added, which are neccessary
        for the git subprocess call. 'git' is added to the first position of the command
        line call. The following kewords are also added in this method.
        "stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines=True" 
        """
        self.cwd_backup = os.getcwd()
        os.chdir(self.cwd)        
        post_args = list(args)
        post_args[0].insert(0, "git")
        post_kwargs = dict(kwargs,
                           stdout = subprocess.PIPE,
                           stderr = subprocess.PIPE,
                           universal_newlines=True,
                          )
        popen_obj = subprocess.Popen(*post_args, **post_kwargs)        
        os.chdir(self.cwd_backup)
        return popen_obj
    

def message_creator (gitcall_obj ,last_modify_date_obj):
    """Creats a commit message out of the information of git status and appends a
    last modify date. The cwd have to be set to the history! 
    input: datetime_obj, last modify date of the notebook. 
    output: string, formated commit message
    """

    stdout_status, stdder_status = gitcall_obj.status_porcelain().communicate()
    status_list = stdout_status.rstrip("\n").split("\n")        # --porcelain returns string with \n seperation. Last element is \n
    commit_message_entries = []
    commit_message_entries.append("modified on "+str(datetime.datetime.date(last_modify_date_obj))+"\n")

    for entry in status_list:
        if entry[0] == "M":
            commit_message_entries.append("modified  "+entry[3:])
        if entry[0] == "A":
            commit_message_entries.append("added     "+entry[3:])
    commit_message = "\n".join(commit_message_entries)
    return commit_message


def validate_diff (gitcall_obj, file):
    """Checks if the file was modified since the last commit.
    input: string, filename. (When cwd is not set, filepath)   
    output: True -> was modified
    """
    
    stdout_diff, stdder_diff = gitcall_obj.diff_name_only(file).communicate()
    if stdout_diff:
        return True


def validate_date (file_path, git_last_com_date_obj):
    """Checks if last commit date equals the last modify date of a file.
    input: string; filepath.
           datetime_obj; eg. git log --pretty=formate:%ci
    output: True -> last modify date unequal last commit date 
    """

    last_modify_date_obj = get_file_date(file_path)
    if datetime.datetime.date(git_last_com_date_obj) != datetime.datetime.date(last_modify_date_obj):
        return True


def get_file_date (file_path):
    """Reads the last modify date out of the file and transform it into a datetime obj.
    input: string; file path.
    output: datetime obj; last modify date of the file.
    """
    
    with codecs.open(file_path,'r',encoding='utf8') as file:
        file_content = file.read()
        header, file_txt =file_content.split('\n', 1)    

    header_list = header.split("|")
    last_modify_date_obj = datetime.datetime.strptime(header_list[5],"%Y-%m-%d %H:%M:%S")
    return last_modify_date_obj


def add_to_commit_next (hist_path,filename):
    """When input results in a new file, append to the.commit_next filename.
    input: string; history_path. 
           string; project part of the entry which is going to be saved.
    output: none
    """

    if not os.path.isfile(os.path.join(hist_path,filename)):
        file_commit = open(os.path.join(hist_path,'.commit_next'), 'a')
        file_commit.write(filename+'\n')   
        file_commit.close()


def create_commit_next (hist_path):
    """Creats a new .commit_next file in the hist_path and appends the current datetime.
    input: string; history_path.
           datetime_obj; current datetime.
    output: none
    """
    current_datetime = experiment.timestamp()
    new_commit_next = open(os.path.join(hist_path,".commit_next"),'w')
    os.chmod(os.path.join(hist_path,".commit_next"),0o777)  # permissions set to rwx for others, after development need to be set to 775 (r-x)!!!!!!!!!!!!!!!!!!!!!
    new_commit_next.write(str(current_datetime)+'\n')
    new_commit_next.close()


def read_commit_next (hist_path):
    """Reads and parses the commit_next file.
    input: string; history path.
    output: tupel, (datetime obj; creation date of the file.
                    list; part numbers)
    """

    commit_next_file = open(os.path.join(hist_path,".commit_next"),'r')
    creation_date = commit_next_file.readline()
    striped_creation_date = creation_date.strip()
    creation_datetime = datetime.datetime.strptime(striped_creation_date,"%Y-%m-%d %H:%M:%S")
  
    entry_list = []
    for line in commit_next_file:
        striped_line = line.strip()
        entry_list.append(striped_line)                                                
    commit_next_file.close()
    return(creation_datetime,entry_list)

    
def commit_msg_readout (gitcall_obj, file_path):
    """Readout git log and create two lists with the modified date and the commit hash.
    The cwd have to be set to the history! 
    input: string; file path
    output: list; commit date.
            list; commit hash.
    """
    
    git_log_obj = gitcall_obj.log_pretty(file_path)
    stdout_log, stderr_log = git_log_obj.communicate()
    stdout_log = stdout_log.replace("modified on ","") # remove descripion from commit message
    commit_list = stdout_log.split("\n")
    commit_date = []
    commit_hash = []

    for commit in commit_list:
        if not commit:
            pass
        else:
            commit = commit.split("#")
            commit_date.append(commit[0])
            commit_hash.append(commit[1])
    return commit_date, commit_hash
    
    
def checkout_old_version (gitcall_obj, commit_hash, file_path):
    """Restores all old versions of the file and saves the content (without header).
    Every commit in commit_hash of the file gets restored and saved as rst and converted
    as html in seperate lists.
    input: list; commit hash.
           string; file path.
    output: list; version content in html.
            list; version content in rst.
    """
    old_ver_rst_content = []
    old_ver_html_content = []        
    for i in range(0,len(commit_hash)):
        
        gitcall_obj.checkout(commit_hash[i], file_path).communicate()
        with codecs.open(file_path,'r',encoding='utf8') as file:
            old_file_content = file.read()
            header, file_content = old_file_content.split('\n', 1) # Cut off header
        
        # convert to html and creat a single line html string for javascript.
        old_content_rst = pypandoc.convert(file_content, 'rst', format = 'html')   # convertion from rst to html
        old_ver_rst_content.append(old_content_rst)

        # For correct insertion into the template, the content has to be a single line.
        old_content_html = file_content.split("\n")
        old_content_html = "".join(old_content_html)    
        old_ver_html_content.append(old_content_html)

    return old_ver_html_content, old_ver_rst_content
    
    
    
def check_current_working_file (commit_date, old_ver_html_content, old_ver_rst_content, file_path):
    """Load current working file and validate the date.
    When last_modify unequal last commit date, save its content and date in the particular list.
    input: list, commit_dates format: Y-m-d.
        list, html content txt.
        list, rst content txt.
        string, file path.
    output: list, commit_dates.
            list, html content txt.
            list, rst content txt.
    """

    last_commit_date = datetime.datetime.strptime(commit_date[0],"%Y-%m-%d")
    if validate_date(file_path,last_commit_date):
        with codecs.open(file_path,'r',encoding='utf8') as file:
            working_content = file.read()
            header_org, file_content_org = working_content.split('\n', 1) # Cut off header
        header_split = header_org.split("|")
        last_modify_date = datetime.datetime.strptime(header_split[5],"%Y-%m-%d %H:%M:%S")
        commit_date.insert(0,str(datetime.datetime.date(last_modify_date))+'*')
                
        original_content_rst = pypandoc.convert(file_content_org, 'rst', format = 'html')   #  convertion from rst to html
        old_ver_rst_content.insert(0,original_content_rst)

        # For correct insertion into the template, the content has to be a single line.
        original_content_html = file_content_org.split("\n")  
        original_content_html = "".join(original_content_html) 
        old_ver_html_content.insert(0,original_content_html)
    return commit_date, old_ver_html_content, old_ver_rst_content