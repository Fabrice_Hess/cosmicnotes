<%def name="show_history(input)">
<% root        = h.url_for( "/" )
import os
import subprocess
import shutil
import pypandoc
import datetime
import difflib
import codecs
import sys
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey

import db_modules
import githandling
import eln_util


  


# Recieve data.
part_file_name, hist_id, from_date, to_date = input.split("|")

# Connection to database
engine = create_engine(eln_util.ElnConfig().db_conn())
connection = engine.connect()
metadata = MetaData()
metadata.bind = engine

# Load Tables
histories = Table('histories', metadata, autoload=True)

# Check database for the hist_id and get the path.
s = histories.select()
s = s.where(histories.c.hist_id == hist_id)
result = connection.execute(s)
out = result.fetchone()
archive_path = eln_util.archive_path()
hist_path = os.path.join(archive_path, out[histories.c.hist_path])
file_path = os.path.join(hist_path,part_file_name)
temp_path = os.path.join(hist_path,"temp")

try:
    # Reads the commit message of the file and extracts all commit dates and hashes.
    gitcall_hist = githandling.GitCall(hist_path)
    commit_date, commit_hash = githandling.commit_msg_readout(gitcall_hist, file_path)
    
    # Creats a new dir "temp" and clones the repository into it.
    os.mkdir(temp_path)  #  Creat a temporary directory
    os.chmod(temp_path,0o777)   #  permissions !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    temp_file_path = os.path.join(temp_path,part_file_name)
    gitcall_hist.clone(temp_path).communicate()
    
    # Restores old versions of the file and saves the content in html and rst. 
    gitcall_temp = githandling.GitCall(temp_path)
    old_ver_html_content, old_ver_rst_content = githandling.checkout_old_version(gitcall_temp, commit_hash, temp_file_path)
    
    # Validates the seleced file for a non commited change and appends it to the date and content list.
    commit_date, old_ver_html_content, old_ver_rst_content = githandling.check_current_working_file(commit_date, old_ver_html_content, old_ver_rst_content, file_path)

finally:
    if os.path.isdir(temp_path):
        shutil.rmtree(temp_path)  #  delete temp directory + content

# Declares which versions are outputed.
if from_date == "first_start" and to_date == "first_start": # called via mako-file
    from_date_ind = len(commit_date)-1
    to_date_ind = 0
    from_date = commit_date[from_date_ind]
    to_date = commit_date[to_date_ind]
else:                                                  # calling the page out of this page
    from_date_ind = commit_date.index(from_date)       
    to_date_ind = commit_date.index(to_date)

# Creats a whole HTML page containing a Diff table.
difflib_obj = difflib.HtmlDiff()
diff_table = difflib_obj.make_file(old_ver_rst_content[from_date_ind].split("\n"),
                                old_ver_rst_content[to_date_ind].split("\n"))
diff_table = diff_table.replace("Courier","Roboto",1) #  changes the font of the table

# Converts the old text list to a string, which can be inserted in an js array. 
old_content_str = eln_util.list_to_jsarray(old_ver_html_content)
    
# Create an insertable option tag for the js dropdown menus containing the commit dates.
dropdown_option_tag = eln_util.list_to_jsoption_tag(range(0,len(commit_date)), commit_date)

# Sets the back_url to the specific path. The user gets back to the same page he comes from. 
if part_file_name[:11] == "annotations":
    back_url="annotation"
else:
    back_url="eln"    
%>
    

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- load css file  -->
        ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/eln.css' )}
        ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/CKeditor_content.css' )}       
        <script type="text/javascript">   
        
console.log(${old_content_str});
        
            var old_html_versions = new Array(
            ${old_content_str}
            );                    
                        
            function Update() {         
                // Reloads the show history page, with the currently appointed 
                // parameters. 
            
                var from_date_form = document.getElementById("dropdown_from"),
                    from_date = from_date_form.options[from_date_form.selectedIndex].text;
            
                var to_date_form = document.getElementById("dropdown_to"),
                    to_date = to_date_form.options[to_date_form.selectedIndex].text;
                
                window.location.href = '${root}visualization/show/eln?'+
                                       'dataset_id='+'${trans.security.encode_id(hda.id)}'+
                                       '&show_history='+"${part_file_name}|${hist_id}|"+from_date+"|"+to_date;
                }
                
        </script>    
    </head>
    <body>
    <div class='page_container'>
        
        <div class='page_eln_header' style='min-height:none;'>
            <div class='page_header_logo_box'>
                 <img src='${root}plugins/visualizations/eln/static/img/pen.png'/>
            </div>
            <div class='page_header_title_box'>
                Show History
            </div>
            <div class='page_header_action_box'>    
                <form action="${root}visualization/show/${back_url}" method="get" id="go_back">
                    <input type="submit" value="Back" class="button">
                    <input type="hidden" name="dataset_id" id="dataset_id" value='${trans.security.encode_id(hda.id)}' class="button">
                </form>  
            </div>
        </div> 
        
        <div class='page_body'>

            <div id="diff_table_head">      
                <div id='diff_table_head_left'>
                    <div class='diff_table_head_el' style='float:left;margin-left:20px;'>
                    <button class='eye_button' title='Show Version' id="show_from" onclick="show_modal('${from_date_ind}')"><img src='${root}plugins/visualizations/eln/static/img/eye.png'/></button>
                    </div>
                    <div class= "diff_table_head_el" style='float:right;margin-right:20px;'>
                        ${from_date}
                    </div> 
                </div>
                <div id='diff_table_head_center'>
                    <div id ="left_dropdown_box">
                        <div class= "diff_table_head_el" style='text-align:center;'>
                            <form>
                                <select id="dropdown_from">
                                    ${dropdown_option_tag}
                                </select>
                            </form>
                        </div>
                    </div>
                    <div id = "pos_update_button" class= "diff_table_head_el">
                        <div class= "diff_table_head_el" style='text-align:center;'>
                            <button class='button' style='float:none;' onclick='Update()'>Update</button>
                        </div>
                    </div>                   
                    <div id = "right_dropdown_box" class= "diff_table_head_el">
                        <div class= "diff_table_head_el" style='text-align:center;'>
                            <form>
                                <select id="dropdown_to">
                                    ${dropdown_option_tag}
                                </select>
                            </form>    
                        </div>    
                    </div>
                </div>       
         
                <div id='diff_table_head_right'>
                    <div class= "diff_table_head_el" style='float:left;margin-left:20px;'>
                        ${to_date}
                    </div>
                    <div class= "diff_table_head_el" style='float:right;margin-right:20px;'>
                    <button class='eye_button' title='Show Version' id="show_from" onclick="show_modal('${to_date_ind}')"><img src='${root}plugins/visualizations/eln/static/img/eye.png'/></button>
                </div>
            </div>       
                
            <div class ="diff_table_window">
                <div id="dic_table" style="float:none">
                    ${diff_table}
                </div>
            </div>
            <!-- Modal -->
            <div id="modal" class="modal">
                <!-- Modal content -->
                <div class="modal-content-show_hist">            
                    <div class="modal-header">
                        <span class="close" id="close_modal">x</span>
                    </div>        
                    <div class="modal-body">                
                        <div style ="margin-left: auto; margin-right: auto;width: 80%px ;">
                            <div id="show_content_modal">
                            </div>
                        </div>         
                    </div>        
                    <div class="modal-footer">     
                    </div>
                </div>
            </div>  
            
        </div>    
    </div>
    <script>
    // Get the modal
    var modal = document.getElementById('modal');
    
    // Get the <span> element that closes the modal
    var close_button_modal = document.getElementById("close_modal");
    
    // When the user clicks on the button, open the modal
    function show_modal (html_content_i) {             
        dialog = document.getElementById("show_content_modal");
        dialog.innerHTML = old_html_versions[html_content_i];
        modal.style.display = "block";
        }   
    
    // When the user clicks on <span> (x), close the modal
    close_button_modal.onclick = function() { 
        modal.style.display = "none";
    }
    document.getElementById("dropdown_from").value = "${from_date_ind}";
    document.getElementById("dropdown_to").value = "${to_date_ind}";
    </script>
        
    </body>
</html>
</%def>
    

