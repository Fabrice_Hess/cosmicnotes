<%
import os
import sys

# Get path to scripts and append to sys.path
template_path = os.path.join(util.galaxy_root_path, self.filename)
template_dir_path = os.path.dirname(template_path)
visu_dir_path = os.path.dirname(template_dir_path)
static_path = os.path.join(visu_dir_path, 'static')
script_path = os.path.join(static_path, 'scripts')
lib_path = os.path.join(static_path, 'lib')

if script_path not in sys.path:
    sys.path.append(script_path)
if lib_path not in sys.path:
    sys.path.append(lib_path)

import db_modules
db_modules.create_tables()

load_editor = True

if trans.request.params.has_key('save_header'):
    import save
    header = trans.request.params['save_header']  
    editor_text = trans.request.params['editor_user_input']     
    save.save(header, editor_text)
    load_editor = False

if trans.request.params.has_key('settings_change'):
    input_data = trans.request.params['settings_change'] 
    db_modules.change_settings(input_data)
    load_editor = False

if trans.request.params.has_key('export'):
    import export
    hist_id = trans.request.params['export'] 
    root = trans.request.params['root'] 
    export.export(hist_id, root, trans)
    load_editor = False
    
if trans.request.params.has_key('annolink'):
    hist_id = trans.request.params['annolink'] 
    root = trans.request.params['root'] 
    user = trans.request.params['user'] 
    annolink_response = db_modules.annolink_other_histories(hist_id, user, root, trans)
    load_editor = False



%>

%if trans.request.params.has_key('annolink'):
    ${annolink_response}

%elif trans.request.params.has_key('overview'):
    <%namespace name="overview" file="overview.mako"/>
    ${overview.overview(trans)}

%elif trans.request.params.has_key('sub_filter'):
    <%namespace name="respond" file="overview_respond.mako"/>
    ${respond.overview_respons(trans)}

%elif trans.request.params.has_key('show_history'):
    <% input_data = trans.request.params['show_history'] %>
    <%namespace name="show_hist" file="show_history.mako"/>
    ${show_hist.show_history(input_data)}
    
%elif trans.request.params.has_key('settings'):
    <% input_data = trans.request.params['settings'] %>
    <%namespace name="settings" file="history_settings.mako"/>
    ${settings.settings(input_data)}

%elif trans.request.params.has_key('annotation'):
    <%namespace name="annotation" file="annotation.mako"/>
    ${annotation.annotation()}

%elif load_editor:
    <%namespace name="eln" file="editor.mako"/>
    ${eln.eln()}
%endif








