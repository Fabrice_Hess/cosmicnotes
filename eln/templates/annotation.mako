<%def name="annotation()">
<%
root        = h.url_for( "/" )

from mako.template import Template

import os  
import json
import sys

import datetime  

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey

import eln_util
import experiment 

# Get the archive path and append the hist_path.
archive_path = eln_util.archive_path()
hist_path = os.path.join(archive_path, str(hda.history.id))

hist_name =  str(hda.history.name)
# escape \ and ' in history names
hist_name = hist_name.replace("\\","\\\\")      
hist_name = hist_name.replace("'","\\'")        
# pipe characters are reserved for use in file headers
hist_name = hist_name.replace("|","/")

# Connection to database
engine = create_engine(eln_util.ElnConfig().db_conn())
connection = engine.connect()
metadata = MetaData()
metadata.bind = engine

# Load Tables
annotations = Table('annotations', metadata, autoload=True)
histories = Table('histories', metadata, autoload=True)

# Check database for the hist_id and if necessary insert current history.
s = histories.select()
s = s.where(histories.c.hist_id == hda.history.id)
result = connection.execute(s)
out = result.fetchone()

if not out:
    connection.execute(histories.
                       insert().
                       values(hist_id=hda.history.id,
                              hist_name=hist_name,
                              user_name=hda.history.user.username,
                              hist_path=hda.history.id 
                             )
                      )         

# Check database for the anno_id and get the path.
s = annotations.select()
s = s.where(annotations.c.anno_id == hda.id)
result = connection.execute(s)
out = result.fetchone()

if out:
    anno_path = os.path.join(hist_path, out[annotations.c.anno_path]+'.html')
else:
    anno_path = ''
    
# contains datafile extensions, which are not displayable via data view
not_sup_ext=["bam","ab1","scf","pbed","sff"]
current_date = experiment.timestamp()

# checks for .lock file in the dir and sets variable for javascript access.    
lock_mode = False
if os.path.exists(os.path.join(hist_path,'.lock')):
    lock_mode = True  

# Creats a json string with the different annotation ids for this history and urls to this pages.
# The CKeditor annolink plugin reads this string and creats an html link. 

anno_path = os.path.join(hist_path,'annotations')
annolink_data_json = ''

anno_list = list(experiment.get_annotations(anno_path))
annolink_data_json = eln_util.create_json_string(anno_list, trans, root)

# Validate the current user.
view_mode = False
if trans.user.username != hda.history.user.username:
    view_mode = True
    
# Validate current hist with displayed hist.
diverse_hist = False
if hda.history.id != trans.history.id:
    diverse_hist = True 
    
# load editor text and file header. Template.render sets up a runtime environment for the template.    
file_path = ''
if os.path.exists(anno_path):
    file_path = os.path.join(anno_path, str(hda.id)+'.html') 
if os.path.exists(file_path):
    saved_file = Template(filename=file_path, input_encoding='utf-8',output_encoding='utf-8')
    file_content = saved_file.render_unicode()       
    header, editor_content = file_content.split('\n', 1)
    anno_info = experiment.Annotation(header, editor_content)     
    start_editor_flagg = False
elif not lock_mode:
    anno_info = experiment.Annotation()
    start_editor_flagg = True  
else:
    anno_info = experiment.Annotation(content = "You already terminally submitted your notebook for this project. There is no annotation for this dataset.")
    start_editor_flagg = False
%>

${h.js(
        'libs/jquery/jquery',
    )}

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
        <title> ${visualization_name} </title>
        <!--import CKEditor -->
        <script type="text/javascript" src="${root}plugins/visualizations/eln/static/ckeditor/ckeditor.js"></script>
        <!-- load css file  -->
        ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/eln.css' )}
        ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/CKeditor_content.css' )}
        <style>
            body {
                background-color: rgba(244,244,244,0.79);
            }
        </style>
    </head>
    <body>        
    <div class='page_eln_header'>
        <div class='page_header_logo_box'>
            <img src='${root}plugins/visualizations/eln/static/img/pen.png'/>
        </div>    
        <div class='page_header_title_box'>
        Annotation
        </div>
        <div class='page_header_action_box'>    
            %if diverse_hist and not view_mode:
                <button class='warning_button' onclick ='GoToCurrentHist()' id='warning_button' title='The displayed history is not the current history. Click here to go to the current history note book.'>!</button>
            %endif
            %if diverse_hist and view_mode:
                <button class='button' onclick ='GoToCurrentHist()' id='other_hist_back_button' title='Go back to the current history note book.'>Go Back</button>
            %endif
            
            <button class='button' onclick = 'GoBackToHist()' id='go_back'>Notebook</button>
            

        </div>
    </div> 
        
    <div class='page_body'>
        <div class='anno_top_title'> ${hist_name} </div>
        <div class='anno_title'>${hda.hid}: ${hda.name}</div>
        <form id="checkbox_close_editor" style="display:none;">
            <input type="checkbox" name="close_editor" value="close_editor" id="close_editor" checked="true">close editor when save
        </form>
        
        <div id= "top_entry_section" class ="entry_header_box" style='margin-top:10px;'>
        
            <!-- Shows the creation Date of the annotation. If creation != last modify show last modify date  -->
            <div id= "show_date" class ="date_header_text">
                Creation Date: ${anno_info.date_created.date()}
            </div>
      
            <div id= "show_last_modify" class ="last_modify_header_text">
                %if anno_info.is_modified(): 
                    Last Modify: ${anno_info.date_last_modified.date()}
                %endif
            </div>
            <button class="button" id="button_edit"                            onclick='StartEditor()'>Edit</button>
            <button class="button" id="close_button"     style="display:none;" onclick="CloseEditor()">Close & Discard</button>
            <button class="button" id="save_button"      style="display:none;" onclick="Save('${anno_info.date_created}','${anno_info.date_last_modified}')" >Save</button>
            <button class='button' id="repo_hist_button" style="display:none;" onclick="GoToShowHistory()">Show History</button>    
       </div>
        
        <div class ="show_window" id="start_box" style='width:90%;'>
            ${anno_info.content}
        </div>
        
        <div style="display:none;" id="hidden_box" >
             ${anno_info.content}
        </div>
          
        <p class="view_data_seperator" style="margin-top:30px;"> View Data </p>
       
        <!-- if current datatype-extension is not in the exclusion list, show new frame with view data content  -->
        %if hda.datatype.file_ext not in not_sup_ext:
        <iframe src= "${root}datasets/${trans.security.encode_id(hda.id)}/display/?preview=True" class ="show_data_window" >
        </iframe>
        %else:
        <div class="show_data_window" style="text-align:center;">
        This datatype is not displayable.
        </div>    
        %endif  
    </div> 
        <script>      
        
            var historyId   = '${hda.history.id}',
                hdaId =  '${hda.id}',
                username = '${hda.history.user.username}',
                useremail = '${hda.history.user.email}',
                history_name = '${hist_name}',
                current_date = '${datetime.datetime.date(current_date)}',
                dataset_name =  '${hda.name}',          
                hist_dataset_number = '${hda.hid}',    
                annolink_transfer = '${annolink_data_json}',
                encoded_dataset_id = '${trans.security.encode_id(hda.id)}'      
                header = document.createElement('div'),
                open_editor = false;
                save_sync = true;
                root = '${root}';
           
            // conversion of Python-level booleans to javascript ones
            lock_mode = ${int(lock_mode)} == 1;
            start_editor_flagg = ${int(start_editor_flagg)} == 1;
            view_mode = ${int(view_mode)} == 1;
            diverse_hist = ${int(diverse_hist)} == 1;
           
            // When current user isn't the owner of the history, start view mode.           
            if(view_mode){
                lockmode = true;
                start_editor_flagg = false;
                document.getElementById("button_edit").style.display = 'none';
            } 
                                  
            //when no file exist and lock mode is not activ: start editor  
            if(start_editor_flagg){StartEditor()}  
     
            // when lock_mode == 1, hide edit button       
            if (lock_mode)  {
                document.getElementById("button_edit").style.display = 'none';
            }            
     
            // Show Hist-button when last modify != creation date
            %if anno_info.is_modified(): 
                document.getElementById("repo_hist_button").style.display = 'block';   
            %endif
            
            // execute Autosave before leaving the page
            window.addEventListener("beforeunload", function(e){
                save_sync = false;
                Autosave ()
            }, false);    
            
            
            
            function Autosave() {    
            // when the checkbox for autosave is correctly set, the function triggers the click event of the save button        
                if(open_editor == true) {    
                    var SaveButton = document.getElementById("save_button");
                    SaveButton.click();
                }
            }   
            
            function CloseEditor() {
            // Executed onclick Close and Discard button
            // Restores old saved entry
            
                selected_box = "start_box";
                CKEDITOR.instances[selected_box].setData(editor_old_content);
                CKEDITOR.instances[selected_box].destroy();
                window.clearInterval(autosave_timer);
                document.getElementById("button_edit").style.display = 'block';
                document.getElementById("close_button").style.display = 'none';
                document.getElementById("save_button").style.display = 'none';
                document.getElementById("checkbox_close_editor").style.display = 'none';
                open_editor = false;
            }
            
            function StartEditor() {
     
                // Replace the <div id="start_box"> with a CKEditor instance
                CKEDITOR.replace('start_box');  
                open_editor = true;    
                editor_old_content = CKEDITOR.instances['start_box'].getData();   
                document.getElementById("button_edit").style.display = 'none';
                document.getElementById("close_button").style.display = 'block';
                document.getElementById("save_button").style.display = 'block';
                document.getElementById("checkbox_close_editor").style.display = 'block';

                // When new editor is opened and timer is running, reset timer.
                if (typeof autosave_timer !== 'undefined'){
                    window.clearInterval(autosave_timer);    
                } 
                // autosave. setInterval executes every 10 min the autosave function
                autosave_timer = window.setInterval(function(){ Autosave() }, 600000);    
            }
    
            function Save(creation_datetime, last_modify_datetime) {
            // executed on submit of form_submit 
        
                var editor_user_input = CKEDITOR.instances.start_box.getData();
                    
                // Checks if the editor is empty and breaks the submit.
                editor_user_input_trim =  editor_user_input.trim();    
                if (editor_user_input_trim === ""){
                    //alert("You have to type something in the editor to save its content!");
                    return false;
                }          
              
                if (editor_old_content.trim() != editor_user_input_trim) {
                    url = '${root}'+'visualization/show/eln?'+
                          'dataset_id='+'${trans.security.encode_id(hda.id)}';
                       
                    save_header = history_name+"|"+dataset_name+"|"+username+"|"+useremail+"|"+creation_datetime+"|"+last_modify_datetime+"|"+historyId+"|"+hist_dataset_number+"|"+hdaId+"|"+encoded_dataset_id+"|annotation";
                        
                    $.ajax( {
                        url:url,
                        async: save_sync,
                        type:   'POST',
                        data: {editor_user_input: editor_user_input,
                               save_header : save_header           
                              }   
                    });         
                }
              
                // When checkbox is activ, close editor
                if (document.getElementsByName("close_editor")[0].checked == true) {     
                    CKEDITOR.instances["start_box"].destroy();
                    open_editor = false; 
                    document.getElementById("button_edit").style.display = 'block';
                    document.getElementById("save_button").style.display = 'none';
                    document.getElementById("checkbox_close_editor").style.display = 'none'; 
                    document.getElementById("close_button").style.display = 'none';
                }   
                
                // Change shown last modify date to current date, when function is executed
                // on a other day than the files creation date and the user changed the editors content.                 
                creation_datetime_date = creation_datetime.substring(0, creation_datetime.length - 9);
                if (creation_datetime_date != current_date) {
                    if (editor_old_content.trim() != editor_user_input_trim) {
                        var last_modify_div = document.getElementById("show_last_modify");
                        last_modify_div.innerHTML = "Last Modify: &nbsp "+current_date ; 
                        document.getElementById("repo_hist_button").style.display = 'block'; 
                    }
                }                     
            }
          
            function BeforeLeavePage () {
                // checks for an opened editor
                if (open_editor == true) {
                
                    var editor_user_input = CKEDITOR.instances.start_box.getData();
                    
                    console.log(editor_old_content);
                    console.log(editor_user_input);
                    if ((editor_user_input.trim() == editor_old_content.trim())||(editor_user_input === "")) {
                        return true;
                    }
           
                    if (confirm("You're about to leave the annotation page with an opened editor.\n Do you want to save its content?") == true) {  
                        save_sync = false;
                        Autosave();  
                        return true;          
                    }
                    else {
                        return false;
                    }  
                }
                else {
                    return true;
                }
            }     
                 
            function GoToCurrentHist() {
                var leave_page = BeforeLeavePage() 
                if(leave_page){
                    
                    url = '${root}'+'api/histories/'+'${trans.security.encode_id(trans.history.id)}'+'/contents';
                    $.ajax( {
                        url:    url,
                        async:  false,
                        type:   'GET',
                        success: function (data) {
                            first_dataset = data[0];
                            dataset_id = first_dataset['id'];
                            
                            console.log(dataset_id);
                            
                            window.location.href = '${root}visualization/show/eln?'+
                                                   'dataset_id='+dataset_id;
                            
                            }
                    });         
                }            
            } 
            
            function GoBackToHist () {
                var leave_page = BeforeLeavePage();
                if(leave_page){
                    window.location.href = '${root}visualization/show/eln?'+
                                           'dataset_id='+'${trans.security.encode_id(hda.id)}';
                }
            }
            
            
                    
            function GoToShowHistory () {
                var leave_page = BeforeLeavePage();
                if(leave_page){
                     window.location.href = '${root}visualization/show/eln?'+
                                            'dataset_id='+'${trans.security.encode_id(hda.id)}'+
                                            '&show_history='+'annotations/${hda.id}.html|${hda.history.id}|first_start|first_start'+'&root=${root}';   
                }
                
            }    
        </script>
    </body>
</html>
</%def>