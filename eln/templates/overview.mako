<%def name="overview(trans)">
<%
import os  
import sys

import datetime 
import json 
import eln_util

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.sql import and_, or_, not_
  
import db_modules

# Get the archive path starting with the mako filename
template_path = os.path.join(util.galaxy_root_path, self.filename)
template_dir_path = os.path.dirname(template_path)
eln_dir_path = os.path.dirname(template_dir_path)

# Append new dir to sys.path and import modules
mod_path = os.path.join(os.path.dirname(eln_dir_path),'eln_modules')
if mod_path not in sys.path:
    sys.path.append(mod_path)

search_author = '' 
search_project = ''
order = "update_time"
sub_filter = 'both'
text_search = ''

if trans.request.params.has_key('author'):
    search_author = trans.request.params['author']
if trans.request.params.has_key('project'):
    search_project = trans.request.params['project']
if trans.request.params.has_key('order'):
    order = trans.request.params['order']
if trans.request.params.has_key('sub_filter'):
    sub_filter = trans.request.params['sub_filter']
if trans.request.params.has_key('text_search'):
    text_search = trans.request.params['text_search']     

# Generates a history dictionary dependent of the user-search and filter.
# The dictionary contains all nessecary information for the multi-panel view. 
histdicts, lb_search_hits, anno_search_hits = db_modules.create_histdict(trans, search_author, search_project,text_search , sub_filter)

# encode search hits
eln_hits = []
for hit in lb_search_hits:
    eln_hits.append(trans.security.encode_id( hit ))

anno_hits = []
for hit in anno_search_hits:
    anno_hits.append(trans.security.encode_id( hit ))
    
# Connection to database
engine = create_engine(eln_util.ElnConfig().db_conn())
connection = engine.connect()
metadata = MetaData()
metadata.bind = engine

# Load Tables
histories = Table('histories', metadata, autoload=True)
annotations = Table('annotations', metadata, autoload=True)
projects = Table('projects', metadata, autoload=True)
projects_assign = Table('projects_assign', metadata, autoload=True)

# Create and append a project list for every history dictionary.
project_dict = {}
for row in connection.execute(projects.select()):
    project_dict[row['project_id']]= row['project_name']

for hist in histdicts:  
    result = connection.execute(projects_assign.select().
                                where(projects_assign.c.hist_id == trans.security.decode_id( hist['id'] )))           
    project_list = []
    for row in result:
        project_list.append(project_dict[row['project_id']])                               
    hist['project_list'] = json.JSONEncoder().encode(project_list)                               

# Create a list of project names that are assigned to at least one history.
project_name_list = []
for row in connection.execute(projects_assign.select()):
    if project_dict[row['project_id']] not in project_name_list:
        project_name_list.append(project_dict[row['project_id']])

# Get an list of all anno ids for the used histories
hist_id_list = []
user_name_list = []
for hist in histdicts:
    hist_id_list.append(trans.security.decode_id(hist['id']))
    if hist['user_name'] not in user_name_list:
        user_name_list.append(hist['user_name'])

results = connection.execute(annotations.
                            select().
                            where(annotations.c.hist_id.in_(hist_id_list))
                           )
anno_id_list = []
for row in results:
    anno_id_list.append(trans.security.encode_id(row[0]))
    
root = h.url_for( "/" )
kwargs = { 'app' : 'app', 'histories' : histdicts, 'includingDeleted' : True, 'order' : "update_time", 'limit' : 100 }

%>    
    ${h.css(
        'base',
        'jquery.rating'
    )}
    
    ${h.js(
        'libs/jquery/jquery',
        'libs/jquery/jquery-ui',
        'libs/require',
        'libs/underscore',
        'bundled/libs.bundled',
    )}

<!DOCTYPE HTML>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
       <!-- load css file  -->
    ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/eln.css' )}
    ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/jquery-ui.css' )}   
    ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/overview_mask.css' )}

    </head>
        <body>
        <script type="text/javascript">
        var search_author = '${search_author}' ;
        var search_project = '${search_project}';
        var ROOT = '${root}'
        var DATASET_ID ='${trans.security.encode_id(hda.id)}' ;
        var anno_id_list = ${json.JSONEncoder().encode(anno_id_list)};
        var author_tags = ${json.JSONEncoder().encode(user_name_list)};
        var project_tags = ${json.JSONEncoder().encode(project_name_list)};
        var lb_search_hits = ${json.JSONEncoder().encode(eln_hits)};
        var anno_search_hits = ${json.JSONEncoder().encode(anno_hits)};
        var order = '${order}';
        var sub_filter = '${sub_filter}';
        var text_search = '${text_search}';

console.log(lb_search_hits);
console.log(anno_search_hits);
        //----------------------------------------------------------Galaxy code 
        
        // global variable, required for following scripts
        var galaxy_config = {root : '${h.url_for( "/" )}'};
          
        // path for requirejs, uses now galaxy scripts.
        // Two addresses are served from Galaxy server. The default script
        // address and the visualisations directory 
        // (recognized by '/plugins/visualizations')   
        require.config({
            baseUrl:  "/static/scripts",
            paths: { config: '/plugins/visualizations/eln/static'},
            shim: {
                "libs/underscore": {exports: "_"},
                "libs/backbone": {
                    deps: [ 'jquery', 'libs/underscore' ],
                    exports: "Backbone"
                }
            },
            map: {
                'mvc/tool/tools': {
                    'templates/tool_form.handlebars'    : 'templates/compiled/tool_form',
                    'templates/tool_search.handlebars'  : 'templates/compiled/tool_search',
                    'templates/panel_section.handlebars': 'templates/compiled/panel_section',
                    'templates/tool_link.handlebars'    : 'templates/compiled/tool_link',
                },
            },
        });
    
        // creats the window.bootstrapped variable, necessary for following galaxy scripts
        %for key in kwargs:
            ( window.bootstrapped = window.bootstrapped || {} )[ '${key}' ] = (
                ${ h.dumps( kwargs[ key ], indent=( 2 if trans.debug else 0 ) ) } );
        %endfor
        define( 'bootstrapped-data', function(){
            return window.bootstrapped;
        });
    
        // creats a new Galaxy object, necessary for following galaxy scripts
        require([ 'require', 'galaxy' ], function( require, galaxy ){
            window.Galaxy = new galaxy.GalaxyApp({
                root            : '${h.url_for( "/" )}',
                loggerOptions   : {}
            });
        });    
    
        define( 'jquery', [], function(){ return jQuery; });
            
        function RenderColumns (histdicts, order) {   
        // calls the galaxy scripts to create and render the multiple view page
            require([
                'config/history-model',
                'mvc/history/multi-panel'
            ], function( HISTORY_MODEL, MULTI_PANEL ){
                $(function(){
                    histories = new HISTORY_MODEL.HistoryCollection(histdicts, {
                        includeDeleted  : true,
                        order           : order,
                        // Deleted current history entry to remove its behaviour
                        // in the sort functionallity
                    });
        
                    multipanel = new MULTI_PANEL.MultiPanelColumns({
                        el                          : $( '#center' ).get(0),
                        histories                   : histories,
                        perPage                     : 100
                    }).render( 0 );
                });
            });
        }
    
        RenderColumns(${histdicts}, order);
                 
        //------------------------------------------------------------Page mask         
          
        function PageMask(){
            AddInputBoxes();
            %for hist in histdicts:                    
                // Checks if history-column exists
                if ($("#"+"history-"+'${hist['id']}').length > 0) {
                    // Get for the history column the children of the list-items class.
                    var datasets = $("#"+"history-"+'${hist['id']}').find(".list-items").children();   
                    // Iterate over every child
                    for ( var i = 0, l = datasets.length; i < l; i++ ) {
                        dataset_id = datasets[i].id.substring(8);
                        // Checks if anno button already exists.
                        if (!$("#"+"anno" + dataset_id).length > 0) {
                            if (_.include( anno_id_list, dataset_id ) ) { 
                                anno_btn = CreateAnnoBtn(dataset_id);
                                // Color button if search hit.
                                if (_.include(anno_search_hits, dataset_id)){
                                    anno_btn.setAttribute('style', 'color:green');
                                }                                
                                // Append button to the primary-actions class in the dataset div.
                                $("#"+datasets[i].id).find(".primary-actions").append(anno_btn);
                            }
                        }                                
                        // Checks if eln button already exists.
                        if (!$("#"+"eln"+'${hist['id']}').length > 0) {
                            //Append button to the history column.
                            eln_button = CreateElnBtn('${hist['id']}', dataset_id);
                            $("#"+'history-'+'${hist['id']}').find(".controls").find(".actions").append(eln_button); 
                            // Color button if search hit.
                            if (_.include(lb_search_hits, '${hist['id']}')){
                                eln_button.setAttribute('style', 'color:green');
                            }
                                                      
                            if ('${hist['submitted']}' == 'y') {
                                lock_icon = CreateLockIcon();
                                $("#"+'history-'+'${hist['id']}').find(".controls").find(".actions").append(lock_icon);    
                            }    
                        }
                    }
                    //Change the history size to display the author, update time and projects.
                    $("#"+'history-'+'${hist['id']}')
                        .find(".controls")
                        .find(".history-size")
                        .html('Author: <a class="author">'+'${hist['user_name']}'+'</a>'+
                              '<br>'+'Update: '+'${hist['update_time']}'.substring(0,10)+
                              '<br>'+ ProjectDisplayGenerator(${hist["project_list"]}));  
                    
                    SetupTextListeners();      
                }
            %endfor
        }           
                    
        function CreateAnnoBtn (dataset_id) {  
            var new_form = document.createElement("a");
                new_form.setAttribute('href',"${root}visualization/show/annotation?dataset_id="+ dataset_id);      
                new_form.setAttribute('id',"anno" + dataset_id);         
                new_form.setAttribute('class',"icon-btn display-btn");
                new_form.setAttribute('title',"Annotation");
    
            var icon = document.createElement("span");
                icon.setAttribute('class',"fa fa-comment-o");
    
            new_form.appendChild(icon);
            return new_form;
        }           
        
        function CreateElnBtn (hist_id, dataset_id) {        
            var new_form = document.createElement("a");
                new_form.setAttribute('href',"${root}visualization/show/eln?dataset_id="+dataset_id);       
                new_form.setAttribute('id',"eln"+hist_id);
                new_form.setAttribute('class',"icon-btn display-btn");
                new_form.setAttribute('title',"Go to the electronic lab notebook.");         
    
            var icon = document.createElement("span");
                icon.setAttribute('class',"fa fa-edit");
    
            new_form.appendChild(icon);
            return new_form;
        }
        
        function CreateLockIcon () {
            var new_form = document.createElement("a");
                new_form.setAttribute('class',"icon-btn display-btn");
                new_form.setAttribute('title',"This eln is already submitted.");
            
            var icon = document.createElement("span");
                icon.setAttribute('class',"fa fa-lock");
            
            new_form.appendChild(icon);
            return new_form;
        }
        
        function ProjectDisplayGenerator (project_list) {
            // Returns an HTML string accordingly to the number of projects
            // assigned to a history.
            if (!project_list.length > 0) {
                return '<font color="red"><i>no project assigned</i></font>'                           
            }
            else if (project_list.length == 1) {
                return 'Project: '+'<a class="project">'+ project_list[0]+ '</a>'  
            }
            else {
                var output = 'Projects: ';
                for (var i = 0; i < project_list.length; i++) {
                    if (i == 0) {
                        output += '<a class="project">'+ project_list[i]+ '</a><ul style="padding-left: 51px;">'
                    }
                    if (i < 3 && i != 0) {
                        output += '<li><a class="project">'+ project_list[i]+ '</a></li>'   
                    }         
                    else if (i == 3 && project_list.length == 4) {
                        output +=  '<li><a class="project">'+ project_list[i]+ '</a></li></ul>'   
                    }           
                    else if (i == 3 && project_list.length > 4) {
                        output += '<li>' + '<span class="span_mouse" title="'+ project_list[i]
                    }
                    else if (i > 3 && project_list.length > 4) {   
                        output += ', '+ project_list[i]
                        if ( i == project_list.length-1) {
                            output += '"><i>...</i></span></li></ul>'
                        }   
                    }
                }
                return output
            }
        }
        
        function AddInputBoxes() {
            
            function ConstructSearchField (search_request) {
                var outer_div = document.createElement("div");
                outer_div.setAttribute('class', "search-control search-input");
                                    
                var new_input = document.createElement("input");
                new_input.setAttribute('id', search_request+"_input");   
                new_input.setAttribute('type', "text"); 
                new_input.setAttribute('placeholder', "search "+search_request);   
                new_input.setAttribute('class', "search-query");               
                
                new_input.setAttribute('data-toggle', "popover_"+search_request);
                new_input.setAttribute('data-trigger', "manual");
                new_input.setAttribute('data-content', "This "+search_request+" doesn't exists.");
                new_input.setAttribute('data-placement', "left");
                
                var new_span = document.createElement("span");
                new_span.setAttribute('class', "search-clear fa fa-times-circle");
                new_span.setAttribute('id', "clear_"+search_request+"_search");
                
                outer_div.appendChild(new_input);
                outer_div.appendChild(new_span);
                
                $('.control-column.control-column-right.flex-column').append(outer_div);
            }
            
            function CreateProjectListIcon () {
                var new_form = document.createElement("a");   
                    new_form.setAttribute('id',"list_projects_btn");
                    new_form.setAttribute('class',"icon-btn display-btn");
                    new_form.setAttribute('title',"Show all projects");         
        
                var icon = document.createElement("span");
                    icon.setAttribute('class',"fa fa-list");
        
                new_form.appendChild(icon);
                $('.control-column.control-column-right.flex-column').append(new_form); 
            }     
            
            function CreateTextSearchIcon () {
                var new_form = document.createElement("a");   
                    new_form.setAttribute('id',"text_search_btn");
                    new_form.setAttribute('class',"icon-btn display-btn");
                    new_form.setAttribute('title',"Text Search");         
        
                var icon = document.createElement("span");
                    icon.setAttribute('class',"fa fa-search");
        
                new_form.appendChild(icon);
                $('.control-column.control-column-right.flex-column').append(new_form); 
            }     
            
            if (!$("#author_input").length > 0) {   
                %if eln_util.ElnConfig().db_is_postgres() or eln_util.ElnConfig().db_is_mysql():    
                CreateTextSearchIcon(); 
                %endif
            
                ConstructSearchField('author');
                ConstructSearchField('project');
                if (search_author != '') {
                    $('#author_input').val(search_author);
                }
                if (search_project != '') {
                    $('#project_input').val(search_project);
                }
                
                CreateProjectListIcon();                
                SetupSearchListeners();
            }
        }
                     
        function SetupSearchListeners() {          
            // Popover
            $('[data-toggle="popover_author"]').popover({delay: {show: 100, hide: 2000}})
            $('[data-toggle="popover_project"]').popover({delay: {show: 100, hide: 2000}})
            
            // Autocomplete
            $("#author_input").autocomplete({
                source: author_tags
            });
            $("#project_input").autocomplete({
                source: project_tags
            });
        
            // Pressing enter 
            $("#author_input").keypress(function (e) {
                if (e.which == 13) {
                    if ( _.contains(author_tags,$('#author_input').val())) {
                        Reload();
                    }
                    else{
                        $("[data-toggle='popover_author']").popover('show');                    
                        $("[data-toggle='popover_author']").popover('toggle');  
                    }
                return false;
                }
            });
            $("#project_input").keypress(function (e) {
                if (e.which == 13) {
                    if ( _.contains(project_tags,$('#project_input').val())) {
                    Reload();
                    }
                    else{
                        $("[data-toggle='popover_project']").popover('show');                    
                        $("[data-toggle='popover_project']").popover('toggle');  
                    }
                return false;
              }
            });
            $("#text_search_input").keypress(function (e){  
                if (e.which == 13) {                    
                    Reload();
                }
            });
            
            // Click on x
            $( "#clear_project_search" ).click(function(){
                if ($('#project_input').val()) {
                    $('#project_input').val('');
                    Reload();
                }
            });
            $( "#clear_author_search" ).click(function(){
                if ($('#author_input').val()) {
                    $('#author_input').val('');
                    Reload();
                }
            });  
            
            // Project list modal Listeners            
            $( "#list_projects_btn" ).click(function(){
                $( "#modal_project_list" ).show();      
                
            }); 
            $( "#close_modal" ).click(function(){
                $( "#modal_project_list" ).hide();
            }); 
                        
            $( "#select_project_from_list_btn" ).click(function(){
                var select_project_list = document.getElementById("select_project_list");
                if(select_project_list.options[select_project_list.selectedIndex] == undefined){ 
                    // TODO: Stop Reload and set for example first option as selected.
                    Reload();
                }
                else {
                    var selection = $('#select_project_list option:selected').text();
                    $('#project_input').val(selection);
                    Reload();
                }
            });     
            
            $("option").bind("dblclick", function(){
                var selection = $('#select_project_list option:selected').text();
                $('#project_input').val(selection);
                Reload();
            });         
                        
            // Text search modal Listeners            
            $( "#text_search_btn" ).click(function(){
                $( "#modal_text_search" ).show();
                $("#text_search_input").focus();
            }); 
            $( "#close_text_search_modal" ).click(function(){
                $( "#modal_text_search" ).hide();
            });             

            $("#search_txt_btn").click(function(){                 
                if ($('#text_search_input').val() == '') {
                    //TODO: Stop Reload
                    Reload();
                }
                else {
                    Reload();
                }
            });

        }  
        
        function SetupTextListeners () {
            SaveOrder ();
            // For unknown reasons the normal html tooltip isn't
            // correctly running on the page. As a replacement the
            // JQuery-ui plugin tooltip is used. However the tooltip
            // modal won't close when the mouse leaves the element.
            // Therefor an own mouseout listener was written.
            $( ".span_mouse" ).tooltip();
            $( ".span_mouse" ).mouseout(function() {
                $( ".tooltip.fade.top.in" ).remove();
            });     
            
            $( ".project" ).click(function(){
                $('#project_input').val($(this).text());
                Reload();
            });
            
            $( ".author" ).click(function(){
                $('#author_input').val($(this).text());
                Reload();
            });
        }
        
        
        function SaveOrder () {
            // Setup a Click listener to the dropdown options.
            // On Click the global 'order' variable  gets assigned to the choosen
            // dropdown option.

            function SetOrder (selection) {
                if (selection == 'most recent first'){
                    order = 'update_time';}   
                else if (selection == 'least recent first'){
                    order = 'update_time-asc';}
                else if (selection == 'name, a to z'){
                    order = 'name';}
                else if (selection == 'name, z to a'){
                    order = 'name-dsc';}
                else if (selection == 'size, large to small'){
                    order = 'size';}
                else if (selection == 'size, small to large'){
                    order = 'size-asc';}           
            }

            var elements =$('.dropdown-menu').children();
            for ( var i = 0, l = elements.length; i < l; i++ ) {
                $(elements[i]).click(function(){            
                    SetOrder($(this).text());                    
                });
            }        
        }
        
        function RemoveElements () {
        // Delete unwanted buttons to restrict the possibility that users
        // bypass the inline stylesheet of this mako and gain access to 
        // certain history functionality on this page.
            $('.download-btn.icon-btn').remove();
            $('.icon-btn.show-selectors-btn').remove();
            $('.icon-btn.edit-btn').remove();
            $('.icon-btn.delete-btn').remove();
            $('.panel-menu.btn-group').remove();
            $('.panel-menu.btn-group').remove();
            $('.pull-left').remove();
            $('.create-new.btn.btn-default').remove();
            $('.done.btn.btn-default').remove();
            $('.history-drop-target-help').remove();
            $('.history-drop-target').remove();
            $('.panel-controls.clear.flex-row').remove();
            $('.errormessage').html("You don't have the permissions to observe this history. The eln and datasets are available when the history gets published.") 
            modal_checkbox_space = $('#include-deleted').parent().parent();
            $('#include-deleted').parent().remove();      
            FilterSubmittedEln(modal_checkbox_space);  
        }
        
        function FilterSubmittedEln (modal_checkbox_element) {
                    
            function CreateLabelBtn (id, text) {
                var new_form = document.createElement("label");
                new_form.innerHTML = "<input type='checkbox' id="+id+">"+text+"<br></input>"
                return new_form
            }
        
            new_form1 = CreateLabelBtn('both', 'Display all lab books');
            new_form2 = CreateLabelBtn('only_submitted', 'Display only submitted lab books');
            new_form3 = CreateLabelBtn('only_non_submitted', 'Display only non submitted lab books');
            modal_checkbox_space.append(new_form1).append(new_form2).append(new_form3);
                
            $("#"+sub_filter).prop("checked", true);
        
            // Setup listeners.
            $("#both").change(function() {
                sub_filter = 'both';
                Reload();
            });
        
            $("#only_submitted").change(function() {
                sub_filter = 'only_submitted';
                Reload();
            });
            
            $("#only_non_submitted").change(function() {
                sub_filter = 'only_non_submitted';
                Reload();
            });
        }
        
        function Reload(){ 
        // Reloads the overview page, with the currently appointed search
        // parameters.
            if ( _.contains(author_tags,$('#author_input').val())) {
            // Delete input, if it's not existing in the database.
                search_author = $('#author_input').val();
            }
            else {search_author = ''}
            
            if ( _.contains(project_tags,$('#project_input').val())) {
                search_project = $('#project_input').val();
            }
            else { search_project = ''}           
            
            text_search = $('#text_search_input').val();
                 
            window.location.href = galaxy_config.root +'visualization/show/eln?'+
                                   'dataset_id='+DATASET_ID+
                                   '&author='+search_author+
                                   '&project='+search_project+
                                   '&order='+order+
                                   '&sub_filter='+sub_filter+
                                   '&text_search='+text_search+
                                   '&overview=';
        }
        
        // TODO: The original page gets adjusted in a certain time interval,
        // because new elements are loaded under unpredictable conditions.       
        setInterval(function() {
            PageMask();
            RemoveElements();
        },200) 
                     
        </script>
            
        <!-- Modal for the project list -->
        <div id="modal_project_list" class="modal">
            <!-- Modal content -->
            <div class="modal-content" style="width: 480px;">            
                <div class="modal-header">
                    Select a project.
                    <span class="close" id="close_modal">X</span>
                </div>        
                <div class="modal-body" style ="margin-top:10px; margin-left:auto; margin-right:auto; width: 420px;">                
                    <select id="select_project_list" size="12" style=" width: 95%;" >
                    </select>                         
                </div>        
                <div class="modal-footer">  
                <button type="button" class="button" id="select_project_from_list_btn" >Select</button>   
                </div>
            </div>
        </div>
            
        <!-- Modal for the text search -->
        <div id="modal_text_search" class="modal">
            <!-- Modal content -->
            <div class="modal-content" style="width: 480px;">            
                <div id="text_search_modal_header" class="modal-header">
                    Please type in your search request.
                    <span class="close" id="close_text_search_modal">X</span>
                </div>        
                <div class="modal-body" style ="margin-top:10px; margin-left:auto; margin-right:auto; width: 420px;">                
                     <input type="text" id='text_search_input' style='width:100%;'>                   
                </div>        
                <div class="modal-footer">  
                <button type="button" class="button" id="search_txt_btn" >Search</button>   
                </div>
            </div>
        </div>
            
            
        <script>
            // Populate modal project list         
            var project_tags = project_tags.sort();                       
            $.each(project_tags, function(key, value) {   
                 $('#select_project_list')
                     .append($("<option></option>")
                     .attr("value",key)
                     .text(value)); 
            }); 
            
            // Populate text search modal
            $('#text_search_input').val(text_search);
               
        // open text-search modal if there was a search query and no hit was found.
        console.log($.isEmptyObject(lb_search_hits) &&
            $.isEmptyObject(anno_search_hits) &&
            text_search != '');
        
        
        if ($.isEmptyObject(lb_search_hits) &&
            $.isEmptyObject(anno_search_hits) &&
            text_search != '') {

            document.getElementById("text_search_modal_header").innerHTML = 'Your search request was not found. <span class="close" id="close_text_search_modal">X</span>';
            $( "#modal_text_search" ).show();                        
        }
        </script>
        
        <div class='page_eln_header'>
            <div class='page_header_logo_box'>
                <img src='${root}plugins/visualizations/eln/static/img/pen.png'/>
            </div>
            <div class='page_header_title_box'>
                Electronic Lab Notebook
            </div>
            <div class='page_header_action_box'>    
                <form action="${root}visualization/show/eln" method="get" id="go_back">
                    <input type="submit" value="Notebook" class="button">
                    <input type="hidden" name="dataset_id" id="dataset_id" value="${trans.security.encode_id(hda.id)}" class="button">
                </form>  
            </div>
        </div> 
        <div id="center"></div>
    </body>
</html>
</%def>
<!--   


                
-->
