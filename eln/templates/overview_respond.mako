<%def name="overview_respons(trans)">
<%
import json
import db_modules

search_author = '' 
search_project = ''
sub_filter = 'both'
if trans.request.params.has_key('author'):
    search_author = trans.request.params['author']
if trans.request.params.has_key('project'):
    search_project = trans.request.params['project']
if trans.request.params.has_key('sub_filter'):
    sub_filter = trans.request.params['sub_filter']
if trans.request.params.has_key('text_search'):
    search_text = trans.request.params['text_search']    

# Generates a history dictionary dependent of the user-search and filter.
# The dictionary contains all nessecary information for the multi-panel view. 
histdicts, lb_search_hits, anno_search_hits = db_modules.create_histdict(trans, search_author, search_project, search_text, sub_filter)

%>
${json.JSONEncoder().encode(histdicts)}
</%def>