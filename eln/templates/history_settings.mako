<%def name="settings(input)">

<%
root = h.url_for( "/" )

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey

import eln_util
import db_modules
    
hist_id = input
# Connection to database
engine = create_engine(eln_util.ElnConfig().db_conn())
connection = engine.connect()
metadata = MetaData()
metadata.bind=engine

# Load Tables
projects_assign = Table('projects_assign', metadata, autoload=True)
projects = Table('projects', metadata, autoload=True)
histories = Table('histories', metadata, autoload=True)

# Get history_name of this history from the database.
s = histories.select()
s = s.where(histories.c.hist_id == hist_id)
result = connection.execute(s)

out = result.fetchone()
history_name = out[histories.c.hist_name]

# Get project_ids of this history from the database.
s = projects_assign.select()
s = s.where(projects_assign.c.hist_id == hist_id)
result = connection.execute(s)

project_ids = []
for row in result:
    project_ids.append(row[projects_assign.c.project_id])
    
# Get project_names that are assigned to this history.
project_names_list = db_modules.get_project_list_of_hist(hist_id)
overview_list_option_tag = eln_util.list_to_jsoption_tag(project_ids, project_names_list)

# Get the other project names, which aren't assigned to this project.
s = projects.select()
s = s.where(projects.c.project_id.notin_(project_ids))
result = connection.execute(s)

other_project_ids = []
other_project_names = []
for row in result:
    other_project_ids.append(row[projects.c.project_id])
    other_project_names.append(row[projects.c.project_name])

add_list_option_tag = eln_util.list_to_jsoption_tag(other_project_ids, other_project_names)

# Get all project names and create a js array string.
all_project_names = []
result = connection.execute(projects.select())
for row in result:
    all_project_names.append(row[projects.c.project_name])

project_names_array = eln_util.list_to_jsarray(all_project_names)
            
%>

${h.js(
        'libs/jquery/jquery',
    )}

<!DOCTYPE HTML>
<html>
    <head> 
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- load css file  -->
        ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/eln.css' )}
        ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/CKeditor_content.css' )}
        <style>
            body { 
            background-color: rgba(244,244,244,0.79);
            }
  
        </style>
        <script>
            
            var all_project_names = new Array(
            ${project_names_array}
            );   
                    
            function AddSelection() { 
                var add_list = document.getElementById("add_list");
                if(add_list.options[add_list.selectedIndex] == undefined){ 
                    add_list.selectedIndex = 0;
                    return false;
                }
                var url = '${root}'+'visualization/show/eln?'+
                    'dataset_id='+'${trans.security.encode_id(hda.id)}'+
                    '&settings_change='+"${hist_id}|"+add_list.options[add_list.selectedIndex].value+"|Ins";
                $.ajax( {
                    url:url,
                    async: false,
                });             
                ReloadSettings();       
            }
                
            function RemoveProject() { 
                var overview_list = document.getElementById("overview_list");
                if(overview_list.options[overview_list.selectedIndex] == undefined){ 
                    overview_list.selectedIndex = 0;
                    return false;
                }                    
                var url = '${root}'+'visualization/show/eln?'+
                    'dataset_id='+'${trans.security.encode_id(hda.id)}'+
                    '&settings_change='+"${hist_id}|"+overview_list.options[overview_list.selectedIndex].value+"|Del";
                        
                $.ajax( {
                    url:url,
                    async: false,
                });
                ReloadSettings();
                
            }
                
            function NewProject() { 
                var new_project_name = document.getElementById("new_txt_input").value;
                
                // input is empty
                if (new_project_name === "") { 
                    return false;
                } 
                // the input project name already exists
                if (all_project_names.indexOf(new_project_name) != -1) { 
                    alert("This project title already exists!");
                    return false;
                }
                
                var url = '${root}'+'visualization/show/eln?'+
                    'dataset_id='+'${trans.security.encode_id(hda.id)}'+
                    '&settings_change='+"${hist_id}|"+new_project_name+"|New";
                        
                $.ajax( {
                    url:url,
                    async: false,
                });
                ReloadSettings();
            }
                
                             
            function HistoryNameChange() { 
                var input_history_name = document.getElementById("history_name_input").value;

                // same name as before                    
                if (input_history_name == '${history_name}') { 
                return false;
                }

                var url = '${root}'+'visualization/show/eln?'+
                    'dataset_id='+'${trans.security.encode_id(hda.id)}'+
                    '&settings_change='+"${hist_id}|"+input_history_name+"|Name";
                        
                $.ajax( {
                    url:url,
                    async: false,
                });
                ReloadSettings();
            }
            
            function ReloadSettings() {
                 window.location.href = '${root}visualization/show/eln?'+
                                   'dataset_id='+'${trans.security.encode_id(hda.id)}'+
                                   '&settings='+'${hist_id}';
            }

        </script>
    </head>
    <body>
    <div class='page_eln_header'>
        <div class='page_header_logo_box'>
            <img src='${root}plugins/visualizations/eln/static/img/pen.png'/>
        </div>    
        <div class='page_header_title_box'>
        Electronic Lab Notebook, History Metadata 
        </div>
        <div class='page_header_action_box'>    
            <form action="${root}visualization/show/eln" method="get" id="go_back">
                <input type="submit" value="Back" class="button">
                <input type="hidden" name="dataset_id" id="dataset_id" value="${trans.security.encode_id(hda.id)}" class="button">
            </form>  
        </div>
    </div> 
    <div class='page_body'>
        <p class="seperator">History Name</p>
        <div class='center_box' style='text-align: center'>
            <div style='display:inline-block;'>
                <div id='info_title' class='question_mark' title='Changes the title of the history in the database. This is only necessary when the eln is submitted. Otherwise the title will be updated to the Galaxy history name.'>?</div>               
            </div>  
            <div style='display:inline-block;'>
                <form autocomplete="off">
                    <input type="text" id="history_name_input" style="display:inline-block;" value="${history_name}">
                </form>
            </div>
            <div style='display:inline-block;'>
                <button onclick='HistoryNameChange()' class ='button' style='float:none;'>Submit</button>
            </div>
        </div>
        <p class="seperator">Projects</p>
        <div style ="margin-left: auto; margin-right: auto;width: 610px ;">
            <form>
                <select id="overview_list" name="overview_list" size="8" style="width:600px;">
                    ${overview_list_option_tag}
                </select>
            </form>
        </div>  
        <div style ="margin-left: auto; margin-right: auto;width: 610px ;">
            <button onclick='RemoveProject()' id="rm_project_button" class='button'>Remove Project</button>
        </div>  
        <!-- Add Project Modal -->
        <!-- Trigger/Open The Modal -->
        <button id="add_mo_button" class="button">Add Project</button>
        <!-- The Modal -->
        <div id="add_Modal" class="modal">
            <!-- Modal content -->
            <div class="modal-content" style='width:60%;'>            
                <div class="modal-header">
                    <span class="close" id="close_add_mo">x</span>
                    <p>Select the project you want to assign to your history.</p>
                </div>        
                <div class="modal-body">                
                    <div style ="margin-left: auto; margin-right: auto;width: 50% ;">
                        <form>
                            <select id="add_list" name="add_list" size="8" style=" width: 100% ;" >
                                ${add_list_option_tag}
                            </select>
                        </form>
                    </div>         
                </div>        
                <div class="modal-footer">
                    <button onclick='AddSelection()' class="button" id="AddSelection_button">Add Project</button>   
                    <button type="button" class="button" id="new_mo_button" >New Project</button>
                </div>
            </div>
        </div>
        <!-- Input New Project Modal -->
        <!-- The Modal -->
        <div id="input_new_Modal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">            
                <div class="modal-header">
                    <span class="close" id="close_input_mo">x</span>
                    <p>Please enter a project name.</p>
                </div>        
                <div class="modal-body">                
                    <div style ="margin-left: auto; margin-right: auto;width: 610px ;">
                        <form autocomplete="off">
                            <input type="text" id="new_txt_input">
                        </form>
                    </div>         
                </div>        
                <div class="modal-footer">
                    <button onclick='NewProject()' class="button" id="new_project_button">Submit</button>                     
                </div>
            </div>
        </div>
    </div>    
        
        
        <script>
            // Get the modal
            var add_modal = document.getElementById('add_Modal'),
                input_modal = document.getElementById('input_new_Modal');
            
            // Get the button that opens the modal
            var add_mo_button = document.getElementById("add_mo_button"),
                new_mo_button = document.getElementById("new_mo_button");
            
            // Get the <span> element that closes the modal
            var close_button_add = document.getElementById("close_add_mo"),
                close_button_new = document.getElementById("close_input_mo");
            
            // When the user clicks on the button, open the modal
            add_mo_button.onclick = function() { 
                add_modal.style.display = "block";
            }
            
            new_mo_button.onclick = function() { 
                add_modal.style.display = "none";
                input_modal.style.display = "block";
                document.getElementById('new_txt_input').focus();              
            }
            
            // When the user clicks on <span> (x), close the modal
            close_button_add.onclick = function() { 
                add_modal.style.display = "none";
            }
            
            close_button_new.onclick = function() { 
                input_modal.style.display = "none";
                add_modal.style.display = "block";
            }

        </script>
    </body>
</html>
</%def>