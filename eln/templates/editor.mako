<%def name="eln()">
<%
    root        = h.url_for( "/" )
    
    import os  
    import json
    import sys
    
    import datetime  
    
    import sqlalchemy
    from sqlalchemy import create_engine
    from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey

    import experiment  
    import eln_util
    import db_modules

    # Get the archive path 
    archive_path = eln_util.archive_path()

    current_date = experiment.timestamp()
    
    hist_name =  str(hda.history.name)
    # escape \ and ' in history names
    hist_name = hist_name.replace("\\","\\\\")      
    hist_name = hist_name.replace("'","\\'")        
    # pipe characters are reserved for use in file headers sent to cgi scripts
    hist_name = hist_name.replace("|","/")

    # Connection to database
    engine = create_engine(eln_util.ElnConfig().db_conn())
    connection = engine.connect()
    metadata = MetaData()
    metadata.bind = engine

    # Load Tables
    projects_assign = Table('projects_assign', metadata, autoload=True)
    projects = Table('projects', metadata, autoload=True)
    histories = Table('histories', metadata, autoload=True)
    
    # Check database for the hist_id and get the path.
    s = histories.select()
    s = s.where(histories.c.hist_id == hda.history.id)
    result = connection.execute(s)
    out = result.fetchone()
    
    if out:
        # Get the archive path starting with the mako filename and append the
        # hist_path of the db result.
        current_labbook_hist_path = os.path.join(archive_path, out[histories.c.hist_path])
    else: 
        connection.execute(histories.
                           insert().
                           values(hist_id=hda.history.id,
                                  hist_name=hist_name,
                                  user_name=hda.history.user.username,
                                  hist_path=hda.history.id, 
                                  creation_time=current_date,
                                  update_time=current_date,
                                  submitted= "n",
                                 )
                          )         
        current_labbook_hist_path = ''
    
    # Get project names, that are assigned to this history. 
    project_list = db_modules.get_project_list_of_hist(hda.history.id)
    project_title = ''
    for project in project_list:
        project_title += project+', '
    project_title = project_title.rstrip(', ')    

    if not project_title:
        project_title = "no project assigned"
     
    file_org={}
    # obtain a list of experiment sections sorted by section number
    if current_labbook_hist_path:
        file_org = sorted(experiment.get_sections(current_labbook_hist_path),
                          key=lambda x: x.section_no)
    else:
        file_org = [] 

    # Creats a json string with the different annotation ids for this history and urls to this pages.
    # The CKeditor annolink plugin reads this file and creats an html link. 
    anno_path = os.path.join(current_labbook_hist_path,'annotations')
    annolink_data_json = ''

    anno_list = list(experiment.get_annotations(anno_path))
    annolink_data_json = eln_util.create_json_string(anno_list, trans, root)

    lock_mode = first_eln_entry = open_new_start_editor = False
    
    # checks for .lock file in the dir and sets variable for javascript access.    
    # check for lock file when crash
    lock_file = os.path.join(current_labbook_hist_path, '.lock')
    if os.path.exists(lock_file):          # try finally
        lock_mode = True 
            
    # Validate the current user.
    view_mode = False
    is_admin = False
    if trans.user.username != hda.history.user.username:
        view_mode = True
    if trans.user_is_admin():    
        is_admin = True
        
    # Validate current hist with displayed hist.
    diverse_hist = False
    if hda.history.id != trans.history.id:
        diverse_hist = True 
        
    # if dict is empty (no txt files), append first entry 
    # elif creation time of the last file != current date, append last part+1    
    if not file_org:
        first_eln_entry = True # first entry, new eln. handle add button visibility
        file_org.append(experiment.Section())  # create first entry in file_org        
        
    elif file_org[-1].date_created.date() != current_date.date():
        if not lock_mode:
            # add new entry unless this labbook is locked
            open_new_start_editor = True  
            
%>

${h.js(
        'libs/jquery/jquery',
    )}

<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> ${visualization_name} </title>
    <!-- import CKEditor -->
    <script type="text/javascript" src="${root}plugins/visualizations/eln/static/ckeditor/ckeditor.js"></script>
    <!-- load css file  -->
    ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/eln.css' )}
    ${h.stylesheet_link( root + 'plugins/visualizations/eln/static/css/CKeditor_content.css' )}
    <style>
        body {
        background-color: rgba(244,244,244,0.79);
        }      
    </style>
  </head>
  <body>  
    <div class='page_container'>
        <div class='page_eln_header'>
            <div class='page_header_logo_box'>
                <img src='${root}plugins/visualizations/eln/static/img/pen.png'/>
            </div>
            <div class='page_header_title_box'>
            Electronic Lab Notebook
            </div>
            <div class='page_header_action_box'>    
                %if diverse_hist and not view_mode:
                    <button class='warning_button' onclick ='GoToCurrentHist()' id='warning_button' title='The displayed history is not the current history. Click here to go to the current history note book.'>!</button>
                %endif
                %if diverse_hist and view_mode:
                    <button class='button' onclick ='GoToCurrentHist()' id='other_hist_back_button' title='Go back to the current history note book.'>Go Back</button>
                %endif
                
                
                <button class='button' onclick ='GoToSettings()' id='settings_button'>Settings</button>   
                <button class='button' onclick ='GoToOverview()'       id='overview_button'>Overview</button>
            </div>
        </div> 
        
        <div class='page_body'>
            <div class='eln_title'>${hist_name}</div>
            <form id="checkbox_close_editor" style="display:none;margin-left:40px;">
                <input type="checkbox" name="close_editor" value="close_editor" id="close_editor" checked="true" >close editor when save
            </form>
        
            <div id = "show_entries">
            
                <div style="margin-left:40px;">
                <b>Author:</b> ${hda.history.user.username} <br>
                <b>Projects:</b> ${project_title}
                </div>
            
                <!-- for every entry in the file_org dictionary, create a form with transferable content -->
                %for section_no, entry in enumerate(file_org, 1):
                    <div id ="entry_box_${section_no}" >           
                        <div id= "top_entry_section" class ="entry_header_box">  
                            <div id= "show_date_${section_no}" class ="date_header_text">Creation Date: ${entry.date_created.date()}</div>
                            <div id= "show_last_modify_${section_no}" class ="last_modify_header_text">
                                %if entry.is_modified():
                                    Last Modify: ${entry.date_last_modified.date()}
                                %endif
                            </div>          
                            %if entry.is_erratum:         
                            <div id = "show_erratum" class = "erratum_header_text">Erratum ${entry.erratum_no}</div>
                            %endif
                            <button class='button' id="repo_hist_button_${section_no}" style="display:none;" onclick="GoToShowHistory(${section_no})">Show History</button>
                            <button class="button" id="button_close_${section_no}"     style="display:none;" onclick="CloseEditor(${section_no})" >Close & Discard</button>
                            <button class="button" id="button_edit_${section_no}"                            onclick="StartEditor(${section_no})">Edit</button>          
                            <button class='button' id="save_button_${section_no}"      style="display:none;" onclick="Save(${section_no}, '${entry.date_created}', '${entry.date_last_modified}')">Save</button>            
                            
                        </div>
                        <div class='editor_container'>
                            <div class ="show_window" id="box_${section_no}">
                                ${entry.content}    
                            </div>
                        </div>
                        <input type="submit" value="Save" id="save_button_${section_no}" class="button" style="display:none;">
                    </div>   
                %endfor
            </div>
        </div>
        <div id='placeholder' style = 'margin:50px'></div>
        <div id = "footer" class="page_footer">
            <div class='page_header_action_box'>
                %if eln_util.ElnConfig().export_enabled():
                    <button class="button" id="export_button"            onclick="Export()"                                style="display:block;" >Export</button> 
                %endif
                <button class="button" id="add_erratum_button"       onclick="AddEntry(erratum = 1)" name="add_button" style="display:none;" >Add Erratum</button> 
                <button class="button" id="add_button"               onclick="AddEntry()"            name="add_button" style="display:block;">Add</button> 
                <button class='button' id='submit_terminally_button' onclick='SubmitTerminally()'                                            >Submit</button>
            </div>
        </div>   
    </div>    
    <script>
         
        var open_editor_no = 0,
            open_box,
            selected_box,
            annolink_transfer = '${annolink_data_json}',
            history_name = '${hist_name}',
            username   = '${hda.history.user.username}',
            useremail = '${hda.history.user.email}',
            historyId =  '${hda.history.id}',
            encoded_hist_id = '${trans.security.encode_id(hda.history.id)}'
            encoded_dataset_id = '${trans.security.encode_id(hda.id)}',
            erratum_part = ${file_org[-1].erratum_no},
            last_part_no = ${len(file_org)},
            last_part_no_modified = last_part_no,
            save_sync = true,
            root = '${root}';
         
        // conversion of Python-level booleans to javascript ones
        lock_mode = ${int(lock_mode)} == 1
        first_eln_entry = ${int(first_eln_entry)} == 1
        open_new_start_editor = ${int(open_new_start_editor)} == 1
        view_mode = ${int(view_mode)} == 1
        is_admin = ${int(is_admin)} == 1      
        diverse_hist = ${int(diverse_hist)} == 1      
        
        function GoToSettings() {
            var leave_page = BeforeLeavePage()   
            if(leave_page){
                window.location.href = '${root}visualization/show/eln?'+
                                       'dataset_id='+encoded_dataset_id+
                                       '&settings='+historyId;
            }
        }       

        function GoToShowHistory(section_no) {
            var leave_page = BeforeLeavePage() 
            if(leave_page){
                window.location.href = '${root}visualization/show/eln?'+
                                       'dataset_id='+'${trans.security.encode_id(hda.id)}'+
                                       '&show_history='+'part_'+section_no+'.html|${hda.history.id}|first_start|first_start'+
                                       '&root=${root}';
            }            
        }        
        
        function GoToOverview() {
            var leave_page = BeforeLeavePage() 
            if(leave_page){
                window.location.href = '${root}visualization/show/eln?'+
                                       'dataset_id='+'${trans.security.encode_id(hda.id)}'+
                                       '&overview=true';
            }            
        }   
        
        function Export() {
            url = '${root}'+'visualization/show/eln?'+
                  'dataset_id='+'${trans.security.encode_id(hda.id)}';
            address =  window.location.protocol + "//" + window.location.host + '${root}';
            $.ajax( {
                url:    url,
                async:  false,
                type:   'GET',

                data: {export: '${trans.security.encode_id(hda.history.id)}',
                       root: address,
                      }  
            });          
        }   
        
        function GoToCurrentHist() {
            var leave_page = BeforeLeavePage() 
            if(leave_page){
                
                url = '${root}'+'api/histories/'+'${trans.security.encode_id(trans.history.id)}'+'/contents';
                $.ajax( {
                    url:    url,
                    async:  false,
                    type:   'GET',
                    success: function (data) {
                        first_dataset = data[0];
                        dataset_id = first_dataset['id'];
                        
                        console.log(dataset_id);
                        
                        window.location.href = '${root}visualization/show/eln?'+
                                               'dataset_id='+dataset_id;
                        
                        }
                });         
            }            
        }   
        
        
        function StartEditor(part_number, scroll_down, close_only, without_autosave) {
        // Start editor in the div "box_part_number". Destroys the last open instance
        // and hides / show the edit buttons. When scroll_down = 1, the page scrolls to
        // the bottom. When close_only = 1, don't start a new editor.
            
            scroll_down = typeof scroll_down !== 'undefined' ? scroll_down : 0;                 // set up default arguments
            close_only = typeof close_only !== 'undefined' ? close_only : 0;
            without_autosave = typeof without_autosave !== 'undefined' ? without_autosave : 0;
            rmv_box = typeof rmv_box !== 'undefined' ? rmv_box : 0;
            
            if (open_editor_no != 0) {
                if ( without_autosave == 0) {
                    Autosave ();                    // before a editor gets closed, save content                    
                }
                open_box = "box_"+open_editor_no;
                var new_editor_user_input = CKEDITOR.instances[selected_box].getData();
                
                CKEDITOR.instances[open_box].destroy();

                document.getElementById("button_edit_"+open_editor_no).style.display = 'block';
                document.getElementById("save_button_"+open_editor_no).style.display = 'none';
                document.getElementById("button_close_"+open_editor_no).style.display = 'none'; 
                
                if (open_editor_no == last_part_no_modified) {                          
                    if (new_editor_user_input === ""){
                        if (typeof section_no != 'undefined'){
                            var added_box = document.getElementById("entry_box_"+section_no);     
                            added_box.parentNode.removeChild(added_box); 
                            // when box is removed set last_part_no_modified back to Last_part_no  
                            last_part_no_modified = last_part_no;                           
                            // reset erratum part number
                            if (erratum_part > 0) {
                                erratum_part -= 1;
                            }                                                                 
                        }
                    }
                    else {
                        // when new box is saved set last_part_no to last_part_no_modified
                        last_part_no = last_part_no_modified;                           
                    }    
                }
                open_editor_no = 0;
            }
            if (close_only == 0) {                                                      // opens editor
                var ckeditor = CKEDITOR.replace('box_'+part_number);
                document.getElementById("button_edit_"+part_number).style.display = 'none';
                document.getElementById("save_button_"+part_number).style.display = 'block';
                document.getElementById("button_close_"+part_number).style.display = 'block';                            
                open_editor_no = part_number;      
                open_editor_old_content = CKEDITOR.instances['box_'+part_number].getData();
                
                // When new editor is opened and timer is running, reset timer.
                if (typeof autosave_timer !== 'undefined'){
                    window.clearInterval(autosave_timer);    
                } 
                // autosave. setInterval executes every 10 min the autosave function
                autosave_timer = window.setInterval(function(){ Autosave() }, ${eln_util.ElnConfig().get_autosave_interval()}); 
            }         
            

            // waits for ckeditor to load completely and scroll to bottom of the page
            if (scroll_down == 1) {
                ckeditor.on("instanceReady",function() {
                    window.scrollTo(0,document.body.scrollHeight); 
                });         
            }
        } 
        
        function CloseEditor(section_no) {
        // Executed onclick Close and Discard button
        // Restores old saved entry

            selected_box = "box_"+section_no;
            CKEDITOR.instances[selected_box].setData(open_editor_old_content);
            StartEditor(section_no, scroll_down = 0, close_only = 1, without_autosave = 1)            
        }
            
        function Save(part_number, creation_datetime, last_modify_datetime) {
        // When the user wants to save the entry, this function creats textareas with
        // the editors content and creats a header, which both can be transfered to the cgi script.   
            
            // Sets the creation_datetime to current date if it is undefiend.
            creation_datetime = typeof creation_datetime !== 'undefined' ? creation_datetime : CurrentDateTime();
            last_modify_datetime = typeof last_modify_datetime !== 'undefined' ? last_modify_datetime : CurrentDateTime();
            
            //Sets the erratum variable if lock mode is active. Ask for confirm.
            //var erratum_part = 0;
            if (lock_mode)  {
                if (confirm("Are you sure you want to save this erratum?\nYou won't be able to edit it afterwards.") == true) {                
                //var erratum_part = ${int(file_org[-1].erratum_no) + 1};
                //var erratum_part = erratum_part + 1;
                
                }
                else {
                return false;
                }
            }
            selected_box = "box_"+part_number;
            editor_user_input = CKEDITOR.instances[selected_box].getData();
            editor_user_input = DecodeHtml(editor_user_input);
              
            // Checks if the editor is empty and breaks the submit.     
            if (editor_user_input === ""){
                //alert("You have to type something in the editor to save its content!");
                return false;
            }          

            if (editor_user_input.trim() != open_editor_old_content.trim()) {
                var project_part = part_number;
                url = '${root}'+'visualization/show/eln?'+
                       'dataset_id='+'${trans.security.encode_id(hda.id)}';     
                save_header = history_name+"|"+username+"|"+useremail+"|"+historyId+"|"+creation_datetime+"|"+last_modify_datetime+"|"+erratum_part+"|"+project_part+"|"+encoded_dataset_id+"|labbook_entry";
                $.ajax( {
                    url:url,
                    async: save_sync,
                    type:   'POST',
                    data: {editor_user_input: editor_user_input,
                           save_header : save_header           
                          }   
                });  
            }
        
            // When eln is empty, show add-button and terminally submit button after first save
            if (first_eln_entry) {                                                       
                document.getElementById("add_button").style.display = 'block';
                document.getElementById("submit_terminally_button").style.display = 'block';
            }
            
            // When checkbox is active close editor, otherwise save the new content in var.
            if (document.getElementsByName("close_editor")[0].checked == true) {     
                StartEditor("part_number", scroll_down = 0, close_only = 1, without_autosave = 1);
            }
            else {
                open_editor_old_content = CKEDITOR.instances['box_'+part_number].getData();
            }
            
            // When lock-mode is active, hide edit button
            if (lock_mode)  {
                document.getElementById("button_edit_"+part_number).style.display = 'none';
            }        
     
            // Change shown last modify date to current date, when function is executed 
            // on a other day than the files creation date and the user changed the editors content.       
            creation_datetime_date = creation_datetime.substring(0, creation_datetime.length - 9);       
            if (creation_datetime_date != CurrentDate()) {
                if (editor_user_input.trim() != open_editor_old_content.trim()) {
                    var last_modify_div = document.getElementById("show_last_modify_"+part_number);
                    last_modify_div.innerHTML = "Last Modify: &nbsp "+CurrentDate() ; 
                    document.getElementById("repo_hist_button_"+part_number).style.display = 'block'; 
                }
            }             
        }
        
        function BeforeLeavePage () {
            // checks for an opened editor
            if (open_editor_no != 0) {
            
                open_box = "box_"+open_editor_no;
                editor_user_input = CKEDITOR.instances[open_box].getData();
                editor_user_input = DecodeHtml(editor_user_input);
                
                if ((editor_user_input.trim() == open_editor_old_content.trim())||(editor_user_input === "")) {
                    return true;
                }
                if (confirm("You're about to leave the your lab book with an opened editor.\n Do you want to save its content?") == true) {   
                        save_sync = false;             
                        Autosave();                                                                              //  Save opened editor 
                        StartEditor('part_number',scroll_down = 0, close_only = 1, without_autosave = 1);       //  close editor   
                        return true;                                                                   
                }
                else {
                    return false;
                }  
            }
            else {
                return true;
            }
        }
    
        function SubmitTerminally () {
            // checks for an opened editor
            if (open_editor_no != 0) {
            
                if (confirm("You're about to terminally submit your lab book with an opened editor.\nDo you want to save its content?") == true) {
                    Autosave();                                                                             //  Save opened editor 
                    StartEditor('part_number',scroll_down = 0, close_only = 1, without_autosave = 1);       //  close editor                                                                 
                }
                else {
                    return false;
                }  
                return false;
            }
            else {
                if (confirm("Are you sure you want to submit this project?\nYou won't be able to edit your notebook afterwards.") == true) { 
                    var last_modify_datetime = CurrentDateTime();
                    url = '${root}'+'visualization/show/eln?'+
                          'dataset_id='+'${trans.security.encode_id(hda.id)}';
                    save_header = history_name+"|"+username+"|"+useremail+"|"+historyId+"|"+CurrentDateTime()+"|"+last_modify_datetime+"|"+erratum_part+"|0"+"|"+encoded_dataset_id+"|terminal_submit";
                    editor_user_input = 'terminal sub';                 
                    $.ajax( {
                        url:url,
                        async: false,
                        type:   'POST',
                        data: {editor_user_input: editor_user_input,
                               save_header : save_header           
                              }   
                    });  
                    window.location.href = '${root}'+'visualization/show/eln?'+
                                           'dataset_id='+'${trans.security.encode_id(hda.id)}'
                }
                else {  
                    return false;        
                } 
            }                    
        }
         
        function AddEntry (erratum) {
        // Onclick add-button this function creats a new empty entry form and new buttons.
        // The entry number is given trough the argument. The creation_date is set to the
        // current date. After this function is called.
        
            erratum = typeof erratum !== 'undefined' ? erratum : 0;   // set up default argument
        
            StartEditor('part_number', scroll_down = 0, close_only = 1, without_autosave = 0);     // close open editor and autosave
        
            section_no = parseInt(last_part_no_modified) + 1 ;
            last_part_no_modified = section_no;
                    
            var save_button = document.createElement("button");
            save_button.setAttribute('id', "save_button_"+section_no); 
            save_button.setAttribute('class', "button");
            save_button.setAttribute('onclick',"Save("+section_no+")"); 
            save_button.innerHTML= "Save";

            var date = document.createElement("div");
            date.setAttribute('id',"date");
            date.setAttribute('class',"date_header_text");
            date.innerHTML = "Creation date: "+CurrentDate();
            
            var editor_container = document.createElement("div");
            editor_container.setAttribute('class', "editor_container");
            
            var txt_field = document.createElement("div");
            txt_field.setAttribute('class', "show_window");
            txt_field.setAttribute('id', "box_"+section_no);
            txt_field.innerHTML = '';  
              
            var close_button = document.createElement("close_button");
            close_button.setAttribute('type', "button");
            close_button.setAttribute('class', "button");
            close_button.setAttribute('id', "button_close_"+section_no);
            close_button.setAttribute('onclick', "CloseEditor("+section_no+")");
            close_button.innerHTML = "Close & Discard";
    
            var edit_button = document.createElement("button");
            edit_button.setAttribute('type', "button");
            edit_button.setAttribute('id', "button_edit_"+section_no);
            edit_button.setAttribute('class', "button");
            edit_button.setAttribute('onclick', "StartEditor("+section_no+")");
            edit_button.innerHTML = 'Edit';  
            
            var show_erratum = document.createElement("div");
            show_erratum.setAttribute('class', "erratum_header_text");           
            show_erratum.innerHTML = 'Erratum '+(erratum_part+1);
            show_erratum.setAttribute('style',"display:none;"); 
            show_erratum.setAttribute('id', "erratum_head_"+section_no);
            
            var new_entry_box = document.createElement("div");
            new_entry_box.setAttribute('id', "entry_box_"+section_no);
            
            var new_top_section = document.createElement("div");
            new_top_section.setAttribute('class', "entry_header_box");
            
            new_top_section.appendChild(date);
            new_top_section.appendChild(show_erratum);
            new_top_section.appendChild(edit_button);
            new_top_section.appendChild(close_button);
            new_top_section.appendChild(save_button);
            new_entry_box.appendChild(new_top_section);
            editor_container.appendChild(txt_field);
            new_entry_box.appendChild(editor_container);
            document.getElementById("show_entries").appendChild(new_entry_box);  
            
            if (erratum == 0 ) {          
                StartEditor(section_no, scroll_down = 1);      
            } 
            else {
                StartEditor(section_no, scroll_down = 1);
                document.getElementById("erratum_head_"+section_no).style.display = 'inline-block';
                erratum_part += 1;
            }
        }        
            
        function Autosave() {    
        // when the checkbox for autosave is correctly set, the function triggers the click event of the save button        

            if (open_editor_no != 0) {    
                if (document.getElementsByName("close_editor")[0].checked == true) {
                    document.getElementById("close_editor").checked = false;
                    var SaveButton = document.getElementById("save_button_"+open_editor_no);
                    SaveButton.click();
                    document.getElementById("close_editor").checked = true;
                }
                else {
                    var SaveButton = document.getElementById("save_button_"+open_editor_no);
                    SaveButton.click();
                }
            }
        }         
                         
        function CurrentDateTime() {
            now = new Date();
            year = "" + now.getFullYear();
            month = "" + (now.getMonth() + 1);  if (month.length == 1) { month = "0" + month; }
            day = "" + now.getDate();           if (day.length == 1) { day = "0" + day; }
            hour = "" + now.getHours();         if (hour.length == 1) { hour = "0" + hour; }
            minute = "" + now.getMinutes();     if (minute.length == 1) { minute = "0" + minute; }
            second = "" + now.getSeconds();     if (second.length == 1) { second = "0" + second; }
            return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
        }    
        
        function CurrentDate() {
            now = new Date();
            year = "" + now.getFullYear();
            month = "" + (now.getMonth() + 1);  if (month.length == 1) { month = "0" + month; }
            day = "" + now.getDate();           if (day.length == 1) { day = "0" + day; }
            return year + "-" + month + "-" + day;
        }      
        
        function DecodeHtml(html) {
            var txt = document.createElement("textarea");
            txt.innerHTML = html;
            return txt.value;
        }
        
        // Show History, visibility 
        %for section_no, entry in enumerate(file_org, 1):
            %if entry.is_modified():
                document.getElementById("repo_hist_button_${section_no}").style.display = 'block';   
            %endif
        %endfor
        
        // View mode activates lock mode
        if(view_mode) {lock_mode= true}
        
        // Start editor for latest part when page is loaded. When lock_mode = 1 the lock mode is initilazed.
        // The editor will not be loaded and all edit-buttons are not displayed. The button for erratum are displayed.   
        if (!lock_mode) {                                                       // lockmode inactive, load processing buttons
            
            if (!open_new_start_editor){
                StartEditor(${len(file_org)}, scroll_down = 1, close_only = 0);
            }
            else {
                var AddButton = document.getElementById("add_button");
                AddButton.click();
            }
            
            if (first_eln_entry) {                                                    // when new history, hide add button, close button and terminal submit button, first save display buttons again     
                document.getElementById("add_button").style.display = 'none';
                document.getElementById("button_close_1").style.display = 'none';
            }
            else {
                document.getElementById("submit_terminally_button").style.display = 'block'; 
            }
            document.getElementById("checkbox_close_editor").style.display = 'block';                     
        }      
        else {                                                                              // lock-mode active, hide processing buttons
            document.getElementById("add_button").style.display = 'none';        
            document.getElementById("submit_terminally_button").style.display = 'none';           
            document.getElementById("add_erratum_button").style.display = 'block';
            %for section_no, entry in enumerate(file_org, 1):
               document.getElementById("button_edit_"+${section_no}).style.display = 'none';   
            %endfor
        }
    
        // View mode doesn't contain add erratum button and settings button
        if(view_mode){
            document.getElementById("add_erratum_button").style.display = 'none';
            if (!is_admin){
                document.getElementById("settings_button").style.display = 'none';
            }  
        }
    
        // alert if history name is undifiend
        if (history_name == "Unnamed history") {
            alert("It is recommended to enter a history name.");
        }
        
        // execute Autosave before leaving the page
        window.addEventListener("beforeunload", function(e){
            save_sync = false;
            Autosave ()
        }, false);     
        
                          
    </script>
  </body>
</html>
</%def>