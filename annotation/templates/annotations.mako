<% root = h.url_for( "/" ) %>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
    </head>
    <body>
        <script>      
            window.location.href = '${root}visualization/show/eln?'+
                                   'dataset_id='+'${trans.security.encode_id(hda.id)}'+
                                   '&annotation=true' ;
        </script>
    </body>
</html>